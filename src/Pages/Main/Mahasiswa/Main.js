/* eslint-disable */
import React, { useState, useEffect, useReducer } from "react";
import { Redirect, useParams } from "react-router-dom";
import { Button, ButtonGroup, Form } from "react-bootstrap";
import axios from "axios";

import Api from "../../../Utils/Api";
import ApiV2 from "../../../Utils/ApiV2";
import ApiOcas from "../../../Utils/ApiOcas";
import { getUser, getToken } from "../../../Utils/Common";
import { CustomAlert } from "../../../Components/CustomAlert";
import { CustomToast } from "../../../Components/CustomToast";
import { CustomLoading } from "../../../Components/CustomLoading";
import CustomHeaderText from "../../../Components/CustomHeaderText";
import { CustomTabs } from "../../../Components/CustomTabs";

const columns = [
  { id: "id", label: "No", minWidth: 90 },
  { id: "nama_lengkap", label: "Nama Lengkap", minWidth: 100 },
  {
    id: "NPK",
    label: "NPK",
    minWidth: 100,
  },
  {
    id: "nama_mk",
    label: "Nama Mata Kuliah",
    minWidth: 170,
  },
  {
    id: "nama_kelas",
    label: "Nama Kelas",
    minWidth: 100,
  },
  {
    id: "angkatan",
    label: "Angkatan",
    minWidth: 100,
  },
  {
    id: "jenis_kelamin",
    label: "Jenis Kelamin",
    minWidth: 100,
  },
  {
    id: "HP",
    label: "HP",
    minWidth: 100,
  },
  {
    id: "email",
    label: "Email",
    minWidth: 100,
  },
  {
    id: "password",
    label: "Password",
    minWidth: 100,
  },
  {
    id: "enroll",
    label: "Enroll",
    minWidth: 100,
  },
  {
    id: "migration",
    label: "Migrasi",
    minWidth: 100,
  },
];

const columnsOcas = [
  { id: "id", label: "No", minWidth: 90 },
  { id: "nama_lengkap", label: "Nama Lengkap", minWidth: 100 },
  {
    id: "NPK",
    label: "NPK",
    minWidth: 100,
  },
  {
    id: "program_id",
    label: "Program ID",
    minWidth: 100,
  },
  {
    id: "angkatan",
    label: "Angkatan",
    minWidth: 100,
  },
  {
    id: "jenis_kelamin",
    label: "Jenis Kelamin",
    minWidth: 100,
  },
  {
    id: "HP",
    label: "HP",
    minWidth: 100,
  },
  {
    id: "email",
    label: "Email",
    minWidth: 100,
  },
  {
    id: "password",
    label: "Password",
    minWidth: 100,
  },
];

const Main = (props) => {
  let params = useParams();
  const [data, setData] = useState([]);
  const [ocas, setOcas] = useState([]);
  const [kelas, setKelas] = useState([]);
  const [kurikulum, setKurikulum] = useState([]);
  const [selectedKelas, setSelectedKelas] = useState("");
  const [modalMsg, setModalMsg] = useState("");
  const [idMahasiswa, setIDMahasiswa] = useState("");
  const [activeButton, setActiveButton] = useState("");
  const [action, setAction] = useState(false);
  const [isReady, setIsReady] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [showToast, setShowToast] = useState(false);
  const [headerText, setHeaderText] = useState("");
  const [bodyText, setBodyText] = useState("");

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    setIsReady(false);
    const token = getToken();
    if (token !== null) {
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
      };
      await axios
        .get(ApiV2.listKelas, config)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            console.log("error", result);
          } else {
            result.data.forEach((o, key) => {
              result.data[key].id_temp = key + 1;
            });
            setKelas(result.data);
          }
        })
        .catch((err) => {
          console.log(err);
        });
      // await axios
      //   .get(ApiOcas.listClasses)
      //   .then((res) => {
      //     const result = res.data;
      //     if (result.msg === "Error") {
      //       console.log("error", result);
      //     } else {
      //       result.data.forEach((o, key) => {
      //         result.data[key].id = key + 1;
      //       });
      //       var dups = [];
      //       result.data.filter(function (el) {
      //         if (dups.indexOf(el.kurikulum) == -1) {
      //           dups.push(el.kurikulum);
      //           return true;
      //         }
      //         return false;
      //       });
      //       setKurikulum(dups);
      //     }
      //   })
      //   .catch((err) => {
      //     console.log(err);
      //   });
      setIsReady(true);
    }
  };

  const fetchMahasiswa = async (e) => {
    setIsLoading(true);
    const temp = kelas.filter((v) => v.id === e);
    const token = getToken();
    const config = {
      headers: {
        "X-Authorization": token.key,
      },
    };
    await axios
      .post(
        ApiV2.listMahasiswa,
        {
          nama_mk: temp[0].nama_mata_kuliah,
          nama_kelas: temp[0].kelas_id,
          program_id: temp[0].program_id,
        },
        config
      )
      .then((res) => {
        const result = res.data;
        if (result.msg === "Error") {
          console.log("error", result);
        } else {
          result.data.forEach((o, key) => {
            result.data[key].id = key + 1;
          });
          setData(result.data);
        }
      })
      .catch((err) => {
        console.log(err);
      });
    await axios
      .post(ApiOcas.listStudentFromKelas, {
        nama_mk: temp[0].nama_mata_kuliah,
        nama_kelas: temp[0].kelas_id,
        program_id: temp[0].program_id,
      })
      .then((res) => {
        const result = res.data;
        if (result.msg === "Error") {
          console.log("error", result);
        } else {
          result.data.forEach((o, key) => {
            result.data[key].id = key + 1;
          });
          setOcas(result.data);
          fetchData();
        }
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => setIsLoading(false));
  };

  const handleShowModal = (event, row) => {
    if (event === "Sync") {
      setModalMsg("Lakukan Sinkronisasi Data Mahasiswa?");
      setActiveButton("Sync");
      setAction(true);
    } else if (event === "Export") {
      setModalMsg("Lakukan Export All as User Data Mahasiswa?");
      setActiveButton("Export");
      setAction(true);
    } else if (event === "Enroll") {
      setModalMsg("Lakukan Enroll Data Mahasiswa?");
      setActiveButton("Enroll");
      setAction(true);
    } else if (event === "Unenroll") {
      setModalMsg("Lakukan Unenroll Data Mahasiswa?");
      setActiveButton("Unenroll");
      setAction(true);
    } else if (event === "DeleteAll") {
      setModalMsg("Lakukan Penghapusan Data Mahasiswa di Dashboard?");
      setActiveButton("DeleteAll");
      setAction(true);
    } else {
      setIDMahasiswa(row.NPK);
      setModalMsg(`Hapus Mahasiswa ${row.nama_lengkap}?`);
      setAction(false);
    }
    setShowModal(true);
  };

  const handleDel = () => {
    setIsLoading(true);
    const token = getToken();
    const user = getUser();
    if (token !== null) {
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
        data: { NPK: idMahasiswa, name: user.name },
      };
      axios
        .delete(ApiV2.deleteMahasiswa, config)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            setShowToast(true);
            setHeaderText("Error");
            setBodyText(result.data);
          } else {
            setShowModal(false);
            setShowToast(true);
            setHeaderText("Success");
            setBodyText(result.data);
            fetchMahasiswa(selectedKelas);
          }
        })
        .catch((err) => {
          alert(err);
          console.log(err);
        })
        .finally(() => setIsLoading(false));
    }
  };

  const handleDelAll = () => {
    setIsLoading(true);
    const token = getToken();
    const user = getUser();
    if (token !== null) {
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
        data: {
          name: user.name,
        },
      };
      axios
        .delete(ApiV2.deleteAllMahasiswa, config)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            setShowToast(true);
            setHeaderText("Error");
            setBodyText(result.data);
          } else {
            setShowModal(false);
            setShowToast(true);
            setHeaderText("Success");
            setBodyText(result.data);
            fetchMahasiswa(selectedKelas);
          }
        })
        .catch((err) => {
          alert(err);
          console.log(err);
        })
        .finally(() => setIsLoading(false));
    }
  };

  const handleSync = () => {
    setIsLoading(true);
    const temp = kelas.filter((v) => v.id === selectedKelas);
    const token = getToken();
    const user = getUser();
    if (token !== null) {
      let url, body;
      if (activeButton === "Sync") {
        url = ApiV2.syncMahasiswa;
        body = {
          name: user.name,
          nama_kelas: temp[0].kelas_id,
          nama_mk: temp[0].nama_mata_kuliah,
          program_id: temp[0].program_id,
        };
      } else if (activeButton === "Enroll") {
        url = ApiV2.enrollMahasiswa;
        body = { name: user.name };
      } else if (activeButton === "Unenroll") {
        url = ApiV2.unenrollMahasiswa;
        body = { name: user.name };
      } else {
        url = ApiV2.exportMahasiswaAsUser;
        body = { name: user.name };
      }
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
      };
      axios
        .post(url, body, config)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            setShowToast(true);
            setHeaderText("Error");
            setBodyText(result.data);
          } else {
            setShowModal(false);
            setShowToast(true);
            setHeaderText("Success");
            setBodyText(result.data);
            fetchMahasiswa(selectedKelas);
          }
        })
        .catch((err) => {
          alert(err);
          console.log(err);
        })
        .finally(() => setIsLoading(false));
    }
  };

  const handleMigrateOne = (e, whichButton) => {
    setIsLoading(true);
    let url;
    if (whichButton === "Ocas") {
      url = ApiV2.syncMahasiswaOne;
    } else {
      url = ApiV2.exportMahasiswaAsUserOne;
    }
    const token = getToken();
    const user = getUser();
    if (token !== null) {
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
      };
      axios
        .post(url, { NPK: e.NPK, name: user.name }, config)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            setShowToast(true);
            setHeaderText("Error");
            setBodyText(result.data);
          } else {
            setShowToast(true);
            setHeaderText("Success");
            setBodyText(result.data);
            fetchMahasiswa(selectedKelas);
          }
        })
        .catch((err) => {
          alert(err);
          console.log(err);
        })
        .finally(() => setIsLoading(false));
    }
  };

  const handleUnenrollOne = (e) => {
    setIsLoading(true);
    let url;
    if (e.enroll === "sudah") {
      url = ApiV2.unenrollMahasiswaOne;
    } else {
      url = ApiV2.enrollMahasiswaOne;
    }
    const token = getToken();
    const user = getUser();
    if (token !== null) {
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
      };
      axios
        .post(url, { NPK: e.NPK, name: user.name }, config)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            setShowToast(true);
            setHeaderText("Error");
            setBodyText(result.data);
          } else {
            setShowToast(true);
            setHeaderText("Success");
            setBodyText(result.data);
            fetchMahasiswa(selectedKelas);
          }
        })
        .catch((err) => {
          alert(err);
          console.log(err);
        })
        .finally(() => setIsLoading(false));
    }
  };

  if (props.isLogin) {
    if (isReady) {
      return (
        <div className="main-container">
          <div className="sub-container">
            <CustomHeaderText title={"Mahasiswa"} />
            <div className="mb-3">
              <ButtonGroup className="me-2" aria-label="Basic example">
                <Button
                  variant={"primary"}
                  disabled={selectedKelas === ""}
                  onClick={() => handleShowModal("Sync")}
                >
                  Sync Data
                </Button>
              </ButtonGroup>
              <ButtonGroup className="me-2" aria-label="Basic example">
                <Button
                  variant={"primary"}
                  onClick={() => handleShowModal("Export")}
                >
                  Export All as User
                </Button>
              </ButtonGroup>
              <ButtonGroup className="me-2" aria-label="Basic example">
                <Button
                  variant={"primary"}
                  onClick={() => handleShowModal("Enroll")}
                >
                  Enroll
                </Button>
              </ButtonGroup>
              <ButtonGroup className="me-2" aria-label="Basic example">
                <Button
                  variant={"primary"}
                  onClick={() => handleShowModal("Unenroll")}
                >
                  Unenroll
                </Button>
              </ButtonGroup>
              <ButtonGroup className="me-2" aria-label="Basic example">
                <Button
                  variant={"primary"}
                  onClick={() => handleShowModal("DeleteAll")}
                >
                  Delete All Data
                </Button>
              </ButtonGroup>
              <ButtonGroup className="me-2" aria-label="Basic example">
                <Form.Select
                  aria-label="Select Kelas"
                  value={selectedKelas}
                  onChange={(e) => {
                    setSelectedKelas(e.target.value);
                    fetchMahasiswa(e.target.value);
                  }}
                >
                  <option>Pilih Kelas</option>
                  {kelas.map((item, index) => (
                    <option value={item.id} key={item.id_temp}>
                      {item.id}
                    </option>
                  ))}
                </Form.Select>
              </ButtonGroup>
            </div>
            <CustomTabs
              columnsOcas={columnsOcas}
              columns={columns}
              ocas={ocas}
              data={data}
              onClick1={(e) => handleShowModal("Delete", e)}
              // onClick2={(e) => history.push(`/mahasiswa/${e.NPK}/update`)}
              // onClick3={(e) => handleMigrateOne(e, "Ocas")}
              onClick4={(e) => handleMigrateOne(e, "Dashboard")}
              onClick5={(e) => handleUnenrollOne(e)}
              // options={kurikulum}
            />
            <CustomToast
              show={showToast}
              headerText={headerText}
              bodyText={bodyText}
              onClose={() => setShowToast(false)}
            />
            <CustomAlert
              show={showModal}
              isLoading={isLoading}
              onHide={() => {
                setShowModal(false);
                setIDMahasiswa("");
              }}
              modalBody={modalMsg}
              showNoButton
              onClickNo={() => {
                setShowModal(false);
                setIDMahasiswa("");
              }}
              onClickYes={() =>
                action
                  ? activeButton === "DeleteAll"
                    ? handleDelAll()
                    : handleSync()
                  : handleDel()
              }
            />
          </div>
        </div>
      );
    } else {
      return <CustomLoading visible={true} />;
    }
  } else {
    return <Redirect to={"/login"} />;
  }
};

export default Main;
