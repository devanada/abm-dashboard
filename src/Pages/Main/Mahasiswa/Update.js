/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import { Redirect, useHistory } from "react-router-dom";
import styled from "styled-components";
import { Form, Col } from "react-bootstrap";
import axios from "axios";

import Api from "../../../Utils/Api";
import { getUser, getToken } from "../../../Utils/Common";
import { CustomInput } from "../../../Components/CustomInput";
import { CustomAlert } from "../../../Components/CustomAlert";
import { CustomButton } from "../../../Components/CustomButton";
import { CustomLoading } from "../../../Components/CustomLoading";
import CustomHeaderText from "../../../Components/CustomHeaderText";

const Update = (props) => {
  let history = useHistory();
  const [nim, setNIM] = useState();
  const [email, setEmail] = useState();
  const [errMsg, setErrMsg] = useState();
  const [kode_kelas, setKodeKelas] = useState();
  const [nama_depan, setNamaDepan] = useState();
  const [nama_belakang, setNamaBelakang] = useState();
  const [isReady, setIsReady] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [validated, setValidated] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      const token = getToken();
      if (token !== null) {
        const config = {
          headers: {
            "X-Authorization": token.key,
          },
        };
        await axios
          .post(Api.detailMahasiswa, { id: props.match.params.id }, config)
          .then((res) => {
            const result = res.data;
            if (result.msg === "Error") {
              setErrMsg(result.data);
              setShowModal(true);
            } else {
              setNamaDepan(result.data.nama_depan);
              setNamaBelakang(result.data.nama_belakang);
              setNIM(result.data.nim);
              setEmail(result.data.email);
              setKodeKelas(result.data.kode_kelas);
            }
            setIsReady(true);
          })
          .catch((err) => {
            console.log(err);
          });
      }
    };
    fetchData();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    const form = e.currentTarget;
    if (form.checkValidity() === false) {
      e.stopPropagation();
    } else {
      const token = getToken();
      const user = getUser();
      let bodyParameters = {
        id: props.match.params.id,
        kode_kelas,
        nama_depan,
        nama_belakang,
        nim,
        email,
        name: user.name,
      };
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
      };
      await axios
        .post(Api.updateMahasiswa, bodyParameters, config)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            console.log(result);
          } else {
            console.log(result);
            history.goBack();
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }

    setIsLoading(false);
    setValidated(true);
  };

  if (props.isLogin) {
    if (isReady) {
      return (
        <div className="main-container">
          <div className="container">
            <Wrapper>
              <CustomHeaderText title={"Update Mahasiswa"} showBack={true} />
              <Form
                noValidate
                validated={validated}
                onSubmit={handleSubmit}
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  width: "40%",
                }}
              >
                <Form.Group as={Col} md="12" controlId="groupKodeKelas">
                  <CustomInput
                    controlId="kodeKelasInput"
                    label="Kode Kelas"
                    required={true}
                    placeholder="Kode Kelas"
                    value={kode_kelas}
                    onChange={(e) => setKodeKelas(e.target.value)}
                    feedbackMsg="Kode kelas tidak boleh kosong"
                  />
                </Form.Group>
                <Form.Group as={Col} md="12" controlId="groupNamaDepan">
                  <CustomInput
                    controlId="namaDepanInput"
                    label="Nama Depan"
                    required={true}
                    placeholder="Nama Depan"
                    value={nama_depan}
                    onChange={(e) => setNamaDepan(e.target.value)}
                    feedbackMsg="Nama depan tidak boleh kosong"
                  />
                </Form.Group>
                <Form.Group as={Col} md="12" controlId="groupNamaBelakang">
                  <CustomInput
                    controlId="namaBelakangInput"
                    label="Nama Belakang"
                    required={true}
                    placeholder="Nama Belakang"
                    value={nama_belakang}
                    onChange={(e) => setNamaBelakang(e.target.value)}
                    feedbackMsg="Nama belakang tidak boleh kosong"
                  />
                </Form.Group>
                <Form.Group as={Col} md="12" controlId="groupNIM">
                  <CustomInput
                    controlId="nimInput"
                    label="NIM"
                    required={true}
                    placeholder="NIM"
                    value={nim}
                    onChange={(e) => setNIM(e.target.value)}
                    feedbackMsg="NIM tidak boleh kosong"
                  />
                </Form.Group>
                <Form.Group as={Col} md="12" controlId="groupEmail">
                  <CustomInput
                    controlId="emailInput"
                    label="Email"
                    required={true}
                    placeholder="Email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    feedbackMsg="Email tidak boleh kosong"
                  />
                </Form.Group>
                <CustomButton label={"Log In"} isLoading={isLoading} />
              </Form>
            </Wrapper>
            <CustomAlert
              show={showModal}
              onHide={() => setShowModal(false)}
              modalBody={errMsg}
              onClickYes={() => setShowModal(false)}
            />
          </div>
        </div>
      );
    } else {
      return <CustomLoading visible={true} />;
    }
  } else {
    return <Redirect to={"/"} />;
  }
};

export default Update;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;
