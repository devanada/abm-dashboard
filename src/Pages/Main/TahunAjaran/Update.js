/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import { Redirect, useHistory } from "react-router-dom";
import styled from "styled-components";
import { Form, Col } from "react-bootstrap";
import axios from "axios";

import Api from "../../../Utils/Api";
import { getUser, getToken } from "../../../Utils/Common";
import { CustomInput } from "../../../Components/CustomInput";
import { CustomAlert } from "../../../Components/CustomAlert";
import { CustomButton } from "../../../Components/CustomButton";
import { CustomLoading } from "../../../Components/CustomLoading";
import CustomHeaderText from "../../../Components/CustomHeaderText";

const Update = (props) => {
  let history = useHistory();
  const [errMsg, setErrMsg] = useState();
  const [kode_tahun_ajaran, setTahunAjar] = useState();
  const [nama_tahun_ajaran, setNamaTahunAjar] = useState();
  const [isReady, setIsReady] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [validated, setValidated] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      const token = getToken();
      if (token !== null) {
        const config = {
          headers: {
            "X-Authorization": token.key,
          },
        };
        await axios
          .post(Api.detailTahunAjaran, { id: props.match.params.id }, config)
          .then((res) => {
            const result = res.data;
            if (result.msg === "Error") {
              console.log("error", result);
            } else {
              setTahunAjar(result.data.kode_tahun_ajaran);
              setNamaTahunAjar(result.data.nama_tahun_ajaran);
            }
            setIsReady(true);
          })
          .catch((err) => {
            console.log(err);
          });
      }
    };
    fetchData();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    const form = e.currentTarget;
    if (form.checkValidity() === false) {
      e.stopPropagation();
    } else {
      const token = getToken();
      const user = getUser();
      let bodyParameters = {
        id: props.match.params.id,
        kode_tahun_ajaran,
        nama_tahun_ajaran,
        name: user.name,
      };
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
      };
      await axios
        .post(Api.updateTahunAjaran, bodyParameters, config)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            setErrMsg(result.data);
            setShowModal(true);
          } else {
            console.log(result);
            history.goBack();
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }

    setIsLoading(false);
    setValidated(true);
  };

  if (props.isLogin) {
    if (isReady) {
      return (
        <div className="main-container">
          <div className="container">
            <Wrapper>
              <CustomHeaderText title={"Update Tahun Ajaran"} showBack={true} />
              <Form
                noValidate
                validated={validated}
                onSubmit={handleSubmit}
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  width: "40%",
                }}
              >
                <Form.Group as={Col} md="12" controlId="groupTahunAjaran">
                  <CustomInput
                    controlId="tahunAjaranInput"
                    label="Tahun Ajaran"
                    required={true}
                    placeholder="Tahun Ajaran"
                    value={kode_tahun_ajaran}
                    onChange={(e) => setTahunAjar(e.target.value)}
                    feedbackMsg="Kode tahun ajaran tidak boleh kosong"
                  />
                </Form.Group>
                <Form.Group as={Col} md="12" controlId="groupNamaTahunAjaran">
                  <CustomInput
                    controlId="namaTahunAjaranInput"
                    label="Nama Tahun Ajaran"
                    required={true}
                    placeholder="Nama Tahun Ajaran"
                    value={nama_tahun_ajaran}
                    onChange={(e) => setNamaTahunAjar(e.target.value)}
                    feedbackMsg="Nama tahun ajaran tidak boleh kosong"
                  />
                </Form.Group>
                <CustomButton label={"Log In"} isLoading={isLoading} />
              </Form>
            </Wrapper>
            <CustomAlert
              show={showModal}
              onHide={() => setShowModal(false)}
              modalBody={errMsg}
              onClickYes={() => setShowModal(false)}
            />
          </div>
        </div>
      );
    } else {
      return <CustomLoading visible={true} />;
    }
  } else {
    return <Redirect to={"/"} />;
  }
};

export default Update;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;
