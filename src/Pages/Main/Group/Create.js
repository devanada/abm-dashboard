import React, { useState } from "react";
import { Redirect, useHistory } from "react-router-dom";
import styled from "styled-components";
import { Form, Col } from "react-bootstrap";
import axios from "axios";

import Api from "../../../Utils/Api";
import { getUser, getToken } from "../../../Utils/Common";
import { CustomInput } from "../../../Components/CustomInput";
import { CustomAlert } from "../../../Components/CustomAlert";
import { CustomButton } from "../../../Components/CustomButton";
import CustomHeaderText from "../../../Components/CustomHeaderText";

const Create = (props) => {
  let history = useHistory();
  const [kode_group, setKodeGroup] = useState();
  const [errMsg, setErrMsg] = useState();
  const [kode_kelas, setKodeKelas] = useState();
  const [nama_group, setNamaGroup] = useState();
  const [visible, setVisible] = useState();
  const [showModal, setShowModal] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [validated, setValidated] = useState(false);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    const form = e.currentTarget;
    if (form.checkValidity() === false) {
      e.stopPropagation();
    } else {
      const token = getToken();
      const user = getUser();
      let bodyParameters = {
        kode_kelas,
        nama_group,
        visible,
        kode_group,
        name: user.name,
      };
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
      };
      await axios
        .post(Api.createGroup, bodyParameters, config)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            setErrMsg(result.data);
            setShowModal(true);
          } else {
            console.log(result);
            history.goBack();
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }

    setIsLoading(false);
    setValidated(true);
  };

  if (props.isLogin) {
    return (
      <div className="main-container">
        <div className="container">
          <Wrapper>
            <CustomHeaderText title={"Buat Mahasiswa Baru"} showBack={true} />
            <Form
              noValidate
              validated={validated}
              onSubmit={handleSubmit}
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                width: "40%",
              }}
            >
              <Form.Group as={Col} md="12" controlId="groupKodeKelas">
                <CustomInput
                  controlId="kodeKelasInput"
                  label="Kode Kelas"
                  required={true}
                  placeholder="Kode Kelas"
                  onChange={(e) => setKodeKelas(e.target.value)}
                  feedbackMsg="Kode kelas tidak boleh kosong"
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="groupNamaGroup">
                <CustomInput
                  controlId="namaGroupInput"
                  label="Nama Group"
                  required={true}
                  placeholder="Nama Group"
                  onChange={(e) => setNamaGroup(e.target.value)}
                  feedbackMsg="Nama group tidak boleh kosong"
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="groupVisible">
                <CustomInput
                  controlId="visibleInput"
                  label="Visible"
                  required={true}
                  placeholder="Visible"
                  onChange={(e) => setVisible(e.target.value)}
                  feedbackMsg="Visible tidak boleh kosong"
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="groupKodeGroup">
                <CustomInput
                  controlId="kodeGroupInput"
                  label="Kode Group"
                  required={true}
                  placeholder="Kode Group"
                  onChange={(e) => setKodeGroup(e.target.value)}
                  feedbackMsg="Kode group tidak boleh kosong"
                />
              </Form.Group>
              <CustomButton label={"Log In"} isLoading={isLoading} />
            </Form>
          </Wrapper>
          <CustomAlert
            show={showModal}
            onHide={() => setShowModal(false)}
            modalBody={errMsg}
            onClickYes={() => setShowModal(false)}
          />
        </div>
      </div>
    );
  } else {
    return <Redirect to={"/"} />;
  }
};

export default Create;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;
