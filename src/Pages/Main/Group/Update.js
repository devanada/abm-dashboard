/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import { Redirect, useHistory } from "react-router-dom";
import styled from "styled-components";
import { Form, Col } from "react-bootstrap";
import axios from "axios";

import Api from "../../../Utils/Api";
import { getUser, getToken } from "../../../Utils/Common";
import { CustomInput } from "../../../Components/CustomInput";
import { CustomAlert } from "../../../Components/CustomAlert";
import { CustomButton } from "../../../Components/CustomButton";
import { CustomLoading } from "../../../Components/CustomLoading";
import CustomHeaderText from "../../../Components/CustomHeaderText";

const Update = (props) => {
  let history = useHistory();
  const [kode_group, setKodeGroup] = useState();
  const [errMsg, setErrMsg] = useState();
  const [kode_kelas, setKodeKelas] = useState();
  const [nama_group, setNamaGroup] = useState();
  const [visible, setVisible] = useState();
  const [isReady, setIsReady] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [validated, setValidated] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      const token = getToken();
      if (token !== null) {
        const config = {
          headers: {
            "X-Authorization": token.key,
          },
        };
        await axios
          .post(Api.detailGroup, { id: props.match.params.id }, config)
          .then((res) => {
            const result = res.data;
            if (result.msg === "Error") {
              setErrMsg(result.data);
              setShowModal(true);
            } else {
              setKodeKelas(result.data.kode_kelas);
              setNamaGroup(result.data.nama_group);
              setVisible(result.data.visible);
              setKodeGroup(result.data.kode_group);
            }
            setIsReady(true);
          })
          .catch((err) => {
            setErrMsg(err);
            setShowModal(true);
            console.log(err);
          });
      }
    };
    fetchData();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    const form = e.currentTarget;
    if (form.checkValidity() === false) {
      e.stopPropagation();
    } else {
      const token = getToken();
      const user = getUser();
      let bodyParameters = {
        id: props.match.params.id,
        kode_kelas,
        nama_group,
        visible,
        kode_group,
        name: user.name,
      };
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
      };
      await axios
        .post(Api.updateGroup, bodyParameters, config)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            console.log(result);
          } else {
            console.log(result);
            history.goBack();
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }

    setIsLoading(false);
    setValidated(true);
  };

  if (props.isLogin) {
    if (isReady) {
      return (
        <div className="main-container">
          <div className="container">
            <Wrapper>
              <CustomHeaderText title={"Update Group"} showBack={true} />
              <Form
                noValidate
                validated={validated}
                onSubmit={handleSubmit}
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  width: "40%",
                }}
              >
                <Form.Group as={Col} md="12" controlId="groupKodeKelas">
                  <CustomInput
                    controlId="kodeKelasInput"
                    label="Kode Kelas"
                    required={true}
                    placeholder="Kode Kelas"
                    value={kode_kelas}
                    onChange={(e) => setKodeKelas(e.target.value)}
                    feedbackMsg="Kode kelas tidak boleh kosong"
                  />
                </Form.Group>
                <Form.Group as={Col} md="12" controlId="groupNamaGroup">
                  <CustomInput
                    controlId="namaGroupInput"
                    label="Nama Group"
                    required={true}
                    placeholder="Nama Group"
                    value={nama_group}
                    onChange={(e) => setNamaGroup(e.target.value)}
                    feedbackMsg="Nama group tidak boleh kosong"
                  />
                </Form.Group>
                <Form.Group as={Col} md="12" controlId="groupVisible">
                  <CustomInput
                    controlId="visibleInput"
                    label="Visible"
                    required={true}
                    value={visible}
                    placeholder="Visible"
                    onChange={(e) => setVisible(e.target.value)}
                    feedbackMsg="Visible tidak boleh kosong"
                  />
                </Form.Group>
                <Form.Group as={Col} md="12" controlId="groupKodeGroup">
                  <CustomInput
                    controlId="kodeGroupInput"
                    label="Kode Group"
                    required={true}
                    value={kode_group}
                    placeholder="Kode Group"
                    onChange={(e) => setKodeGroup(e.target.value)}
                    feedbackMsg="Kode group tidak boleh kosong"
                  />
                </Form.Group>
                <CustomButton label={"Log In"} isLoading={isLoading} />
              </Form>
            </Wrapper>
            <CustomAlert
              show={showModal}
              onHide={() => setShowModal(false)}
              modalBody={errMsg}
              onClickYes={() => setShowModal(false)}
            />
          </div>
        </div>
      );
    } else {
      return <CustomLoading visible={true} />;
    }
  } else {
    return <Redirect to={"/"} />;
  }
};

export default Update;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;
