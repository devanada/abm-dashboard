/* eslint-disable */
import React, { useState, useEffect } from "react";
import { Redirect, useHistory } from "react-router-dom";
import { Button, ButtonGroup } from "react-bootstrap";
import axios from "axios";

import Api from "../../../Utils/Api";
import ApiV2 from "../../../Utils/ApiV2";
import ApiOcas from "../../../Utils/ApiOcas";
import { getUser, getToken } from "../../../Utils/Common";
import { CustomAlert } from "../../../Components/CustomAlert";
import { CustomToast } from "../../../Components/CustomToast";
import { CustomLoading } from "../../../Components/CustomLoading";
import CustomHeaderText from "../../../Components/CustomHeaderText";
import { CustomTabs } from "../../../Components/CustomTabs";

const columns = [
  { id: "id", label: "No", minWidth: 90 },
  {
    id: "nama_mata_kuliah",
    label: "Nama Mata Kuliah",
    minWidth: 100,
  },
  {
    id: "semester",
    label: "Semester",
    minWidth: 100,
  },
  { id: "kelas_id", label: "Kelas ID", minWidth: 100 },
  { id: "program_id", label: "Program ID", minWidth: 100 },
  {
    id: "jam_mulai",
    label: "Jam Mulai",
    minWidth: 100,
  },
  { id: "status_matakuliah", label: "Status Mata Kuliah", minWidth: 100 },
  { id: "jumlah_sks", label: "Jumlah SKS", minWidth: 100 },
  { id: "hari", label: "Hari", minWidth: 100 },
  {
    id: "migration",
    label: "Migrasi",
    minWidth: 100,
  },
];

const columnsOcas = [
  { id: "id", label: "No", minWidth: 90 },
  {
    id: "nama_mata_kuliah",
    label: "Nama Mata Kuliah",
    minWidth: 100,
  },
  {
    id: "semester",
    label: "Semester",
    minWidth: 100,
  },
  { id: "kelas_id", label: "Kelas ID", minWidth: 100 },
  { id: "program_id", label: "Program ID", minWidth: 100 },
  {
    id: "jam_mulai",
    label: "Jam Mulai",
    minWidth: 100,
  },
  {
    id: "jam_selesai",
    label: "Jam Selesai",
    minWidth: 100,
  },
  {
    id: "status_matakuliah",
    label: "Status Mata Kuliah",
    minWidth: 100,
  },
  { id: "jumlah_sks", label: "Jumlah SKS", minWidth: 100 },
  { id: "hari", label: "Hari", minWidth: 100 },
];

const Main = (props) => {
  let history = useHistory();
  const [data, setData] = useState([]);
  const [ocas, setOcas] = useState([]);
  const [idKelas, setIDKelas] = useState("");
  const [modalMsg, setModalMsg] = useState("");
  const [activeButton, setActiveButton] = useState("");
  const [action, setAction] = useState(false);
  const [isReady, setIsReady] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [showToast, setShowToast] = useState(false);
  const [headerText, setHeaderText] = useState("");
  const [bodyText, setBodyText] = useState("");

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    setIsReady(false);
    const token = getToken();
    if (token !== null) {
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
      };
      await axios
        .get(ApiV2.listKelas, config)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            console.log("error", result);
          } else {
            result.data.forEach((o, key) => {
              result.data[key].id = key + 1;
            });
            setData(result.data);
          }
        })
        .catch((err) => {
          console.log(err);
        });
      await axios
        .get(ApiOcas.listClasses)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            console.log("error", result);
          } else {
            result.data.forEach((o, key) => {
              result.data[key].id = key + 1;
            });
            setOcas(result.data);
          }
        })
        .catch((err) => {
          console.log(err);
        });
      setIsReady(true);
    }
  };

  const handleShowModal = (event, row) => {
    if (event === "Sync") {
      setModalMsg("Lakukan sinkronisasi data Kelas?");
      setActiveButton("Sync");
      setAction(true);
    } else if (event === "Export") {
      setModalMsg("Lakukan export data Kelas?");
      setActiveButton("Export");
      setAction(true);
    } else if (event === "DeleteAll") {
      setModalMsg("Lakukan penghapusan data Kelas di dashboard?");
      setActiveButton("DeleteAll");
      setAction(true);
    } else {
      setIDKelas(row.jadwal_kuliah_detail_id);
      setModalMsg(`Hapus Kelas ${row.nama_mata_kuliah}?`);
      setAction(false);
    }
    setShowModal(true);
  };

  const handleDel = () => {
    setIsLoading(true);
    const token = getToken();
    const user = getUser();
    if (token !== null) {
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
        data: { jadwal_kuliah_detail_id: idKelas, name: user.name },
      };
      axios
        .delete(ApiV2.deleteKelas, config)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            setShowToast(true);
            setHeaderText("Error");
            setBodyText(result.data);
          } else {
            setShowModal(false);
            setShowToast(true);
            setHeaderText("Success");
            setBodyText(result.data);
            fetchData();
          }
        })
        .catch((err) => {
          console.log(err);
        })
        .finally(() => setIsLoading(false));
    }
  };

  const handleDelAll = () => {
    setIsLoading(true);
    const token = getToken();
    const user = getUser();
    if (token !== null) {
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
        data: {
          name: user.name,
        },
      };
      axios
        .delete(ApiV2.deleteAllKelas, config)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            setShowToast(true);
            setHeaderText("Error");
            setBodyText(result.data);
          } else {
            setShowModal(false);
            setShowToast(true);
            setHeaderText("Success");
            setBodyText(result.data);
            fetchData();
          }
        })
        .catch((err) => {
          console.log(err);
        })
        .finally(() => setIsLoading(false));
    }
  };

  const handleSync = () => {
    setIsLoading(true);
    const token = getToken();
    const user = getUser();
    if (token !== null) {
      let url;
      if (activeButton === "Sync") {
        url = ApiV2.syncKelas;
      } else {
        url = ApiV2.exportKelas;
      }
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
      };
      axios
        .post(url, { name: user.name }, config)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            setShowToast(true);
            setHeaderText("Error");
            setBodyText(result.data);
          } else {
            setShowModal(false);
            setShowToast(true);
            setHeaderText("Success");
            setBodyText(result.data);
            fetchData();
          }
        })
        .catch((err) => {
          console.log(err);
        })
        .finally(() => setIsLoading(false));
    }
  };

  const handleMigrateOne = (e, whichButton) => {
    setIsLoading(true);
    let url;
    if (whichButton === "Ocas") {
      url = ApiV2.syncKelasOne;
    } else {
      url = ApiV2.exportKelasOne;
    }
    const token = getToken();
    const user = getUser();
    if (token !== null) {
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
      };
      axios
        .post(
          url,
          {
            jadwal_kuliah_detail_id: e.jadwal_kuliah_detail_id,
            name: user.name,
          },
          config
        )
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            setShowToast(true);
            setHeaderText("Error");
            setBodyText(result.data);
          } else {
            setShowToast(true);
            setHeaderText("Success");
            setBodyText(result.data);
            fetchData();
          }
        })
        .catch((err) => {
          console.log(err);
        })
        .finally(() => setIsLoading(false));
    }
  };

  if (props.isLogin) {
    if (isReady) {
      return (
        <div className="main-container">
          <div className="container">
            <CustomHeaderText title={"Kelas"} />
            <div className="mb-3">
              <ButtonGroup className="me-2" aria-label="Basic example">
                <Button
                  variant={"primary"}
                  onClick={() => handleShowModal("Sync")}
                >
                  Sync Data
                </Button>
              </ButtonGroup>
              <ButtonGroup className="me-2" aria-label="Basic example">
                <Button
                  variant={"primary"}
                  onClick={() => handleShowModal("Export")}
                >
                  Export Data
                </Button>
              </ButtonGroup>
              <ButtonGroup className="me-2" aria-label="Basic example">
                <Button
                  variant={"primary"}
                  onClick={() => handleShowModal("DeleteAll")}
                >
                  Delete All Data
                </Button>
              </ButtonGroup>
            </div>
            <CustomTabs
              columnsOcas={columnsOcas}
              columns={columns}
              ocas={ocas}
              data={data}
              menu="kelas"
              onClick1={(e) => handleShowModal("Delete", e)}
              // onClick2={(e) =>
              //   history.push(`/kelas/${e.jadwal_kuliah_detail_id}/update`)
              // }
              onClick3={(e) => handleMigrateOne(e, "Ocas")}
              onClick4={(e) => handleMigrateOne(e, "Dashboard")}
            />
            <CustomToast
              show={showToast}
              headerText={headerText}
              bodyText={bodyText}
              onClose={() => setShowToast(false)}
            />
            <CustomAlert
              show={showModal}
              isLoading={isLoading}
              onHide={() => {
                setShowModal(false);
                setIDKelas("");
              }}
              modalBody={modalMsg}
              showNoButton
              onClickNo={() => {
                setShowModal(false);
                setIDKelas("");
              }}
              onClickYes={() =>
                action
                  ? activeButton === "DeleteAll"
                    ? handleDelAll()
                    : handleSync()
                  : handleDel()
              }
            />
          </div>
        </div>
      );
    } else {
      return <CustomLoading visible={true} />;
    }
  } else {
    return <Redirect to={"/login"} />;
  }
};

export default Main;
