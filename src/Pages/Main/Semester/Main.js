/* eslint-disable */
import React, { useState, useEffect } from "react";
import { Redirect, useHistory } from "react-router-dom";
import { Spinner, Tabs, Tab, Button, ButtonGroup } from "react-bootstrap";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import axios from "axios";

import Api from "../../../Utils/Api";
import { getUser, getToken } from "../../../Utils/Common";
import { CustomAlert } from "../../../Components/CustomAlert";
import { CustomToast } from "../../../Components/CustomToast";
import { CustomLoading } from "../../../Components/CustomLoading";
import CustomHeaderText from "../../../Components/CustomHeaderText";
import CustomTableHead from "../../../Components/TableHead";

const columns = [
  { id: "id", label: "No", minWidth: 90 },
  { id: "nama_semester", label: "Nama\u00a0Semester", minWidth: 170 },
  {
    id: "kode_semester",
    label: "Kode\u00a0Semester",
    minWidth: 100,
  },
  { id: "nama_tahun_ajaran", label: "Tahun\u00a0Ajaran", minWidth: 100 },
  {
    id: "kode_tahun_ajaran",
    label: "Kode\u00a0Tahun\u00a0Ajaran",
    minWidth: 100,
  },
  {
    id: "migration",
    label: "Migrasi",
    minWidth: 100,
  },
];

const useStyles = makeStyles({
  root: {
    width: "100%",
  },
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1,
  },
  container: {
    // maxHeight: 440,
  },
});

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

const Main = (props) => {
  let history = useHistory();
  const classes = useStyles();
  const [data, setData] = useState([]);
  const [ocas, setOcas] = useState([]);
  const [order, setOrder] = useState("asc");
  const [orderBy, setOrderBy] = useState("id");
  const [modalMsg, setModalMsg] = useState("");
  const [idSemester, setIDSemester] = useState("");
  const [activeButton, setActiveButton] = useState("");
  const [action, setAction] = useState(false);
  const [isReady, setIsReady] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [showToast, setShowToast] = useState(false);
  const [headerText, setHeaderText] = useState("");
  const [bodyText, setBodyText] = useState("");
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    setIsReady(false);
    const token = getToken();
    if (token !== null) {
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
      };
      await axios
        .post(Api.listSemester, {}, config)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            console.log("error", result);
          } else {
            setData(result.data);
            setOcas(result.ocas);
          }
          setIsReady(true);
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = rows.map((n) => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const handleShowModal = (event, row) => {
    if (event === "Sync") {
      setModalMsg("Lakukan sinkronisasi data Semester?");
      setActiveButton("Sync");
      setAction(true);
    } else if (event === "Export") {
      setModalMsg("Lakukan export data Semester?");
      setActiveButton("Export");
      setAction(true);
    } else {
      setIDSemester(row.id);
      setModalMsg(`Hapus semester ${row.kode_semester}?`);
      setAction(false);
    }
    setShowModal(true);
  };

  const handleDel = () => {
    setIsLoading(true);
    const token = getToken();
    const user = getUser();
    if (token !== null) {
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
      };
      axios
        .post(Api.deleteSemester, { id: idSemester, name: user.name }, config)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            console.log("error", result);
          } else {
            console.log(result);
            window.location.reload();
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }
    setIsLoading(false);
  };

  const handleSync = () => {
    setIsLoading(true);
    const token = getToken();
    const user = getUser();
    if (token !== null) {
      let url;
      if (activeButton === "Sync") {
        url = Api.asyncOcasSemester;
      } else {
        url = Api.asyncSemester;
      }
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
      };
      axios
        .post(url, { name: user.name }, config)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            alert(result.data);
            console.log("error", result);
          } else {
            setShowModal(false);
            setShowToast(true);
            setHeaderText("Success");
            setBodyText(result.data);
            fetchData();
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }
    setTimeout(() => setIsLoading(false), 1000);
  };

  if (props.isLogin) {
    if (isReady) {
      return (
        <div className="main-container">
          <div className="container">
            <CustomHeaderText title={"Semester"} />
            <div className="mb-3">
              <ButtonGroup className="me-2" aria-label="Basic example">
                <Button
                  variant={"primary"}
                  onClick={() => handleShowModal("Sync")}
                >
                  Sync Data
                </Button>
              </ButtonGroup>
              <ButtonGroup className="me-2" aria-label="Basic example">
                <Button
                  variant={"primary"}
                  onClick={() => handleShowModal("Export")}
                >
                  Export Data
                </Button>
              </ButtonGroup>
            </div>
            <Tabs defaultActiveKey="ocas" id="abm-tab" className="mb-3">
              <Tab eventKey="ocas" title="Ocas">
                <TableContainer className={classes.container}>
                  <Table aria-label="sticky table">
                    <CustomTableHead
                      classes={classes}
                      order={order}
                      orderBy={orderBy}
                      onSelectAllClick={handleSelectAllClick}
                      onRequestSort={handleRequestSort}
                      rowCount={ocas.length}
                      columns={columnsOcas}
                    />
                    <TableBody>
                      {stableSort(ocas, getComparator(order, orderBy))
                        .slice(
                          page * rowsPerPage,
                          page * rowsPerPage + rowsPerPage
                        )
                        .map((row) => {
                          return (
                            <TableRow
                              hover
                              role="checkbox"
                              tabIndex={-1}
                              key={row.id}
                            >
                              {columns.map((column) => {
                                const value = row[column.id];
                                return (
                                  <>
                                    <TableCell
                                      key={column.id}
                                      align={column.align}
                                    >
                                      {value}
                                    </TableCell>
                                  </>
                                );
                              })}
                            </TableRow>
                          );
                        })}
                    </TableBody>
                  </Table>
                </TableContainer>
                <TablePagination
                  rowsPerPageOptions={[10, 25, 100]}
                  component="div"
                  count={ocas.length}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  onPageChange={handleChangePage}
                  onRowsPerPageChange={handleChangeRowsPerPage}
                />
              </Tab>
              <Tab eventKey="dashboard" title="Dashboard">
                <TableContainer className={classes.container}>
                  <Table aria-label="sticky table">
                    <CustomTableHead
                      classes={classes}
                      order={order}
                      orderBy={orderBy}
                      onSelectAllClick={handleSelectAllClick}
                      onRequestSort={handleRequestSort}
                      rowCount={data.length}
                      showButton={true}
                      columns={columns}
                    />
                    <TableBody>
                      {stableSort(data, getComparator(order, orderBy))
                        .slice(
                          page * rowsPerPage,
                          page * rowsPerPage + rowsPerPage
                        )
                        .map((row) => {
                          return (
                            <TableRow
                              hover
                              role="checkbox"
                              tabIndex={-1}
                              key={row.id}
                            >
                              {columns.map((column) => {
                                const value = row[column.id];
                                return (
                                  <>
                                    <TableCell
                                      key={column.id}
                                      align={column.align}
                                    >
                                      {value}
                                    </TableCell>
                                  </>
                                );
                              })}
                              <TableCell>
                                <IconButton
                                  color="secondary"
                                  aria-label="delete"
                                  onClick={() => handleShowModal("Delete", row)}
                                >
                                  <DeleteIcon />
                                </IconButton>
                              </TableCell>
                              <TableCell>
                                <IconButton
                                  color="primary"
                                  aria-label="edit"
                                  onClick={() =>
                                    history.push(`/semester/${row.id}/update`)
                                  }
                                >
                                  <EditIcon />
                                </IconButton>
                              </TableCell>
                            </TableRow>
                          );
                        })}
                    </TableBody>
                  </Table>
                </TableContainer>
                <TablePagination
                  rowsPerPageOptions={[10, 25, 100]}
                  component="div"
                  count={data.length}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  onPageChange={handleChangePage}
                  onRowsPerPageChange={handleChangeRowsPerPage}
                />
              </Tab>
            </Tabs>
            <CustomToast
              show={showToast}
              headerText={headerText}
              bodyText={bodyText}
              onClose={() => setShowToast(false)}
            />
            <CustomAlert
              show={showModal}
              isLoading={isLoading}
              onHide={() => {
                setShowModal(false);
                setIDSemester("");
                ("");
              }}
              modalBody={modalMsg}
              showNoButton
              onClickNo={() => {
                setShowModal(false);
                setIDSemester("");
                ("");
              }}
              onClickYes={() => (action ? handleSync() : handleDel())}
            />
          </div>
        </div>
      );
    } else {
      return <CustomLoading visible={true} />;
    }
  } else {
    return <Redirect to={"/login"} />;
  }
};

export default Main;
