import React, { useState } from "react";
import { Redirect, useHistory } from "react-router-dom";
import styled from "styled-components";
import { Form, Col } from "react-bootstrap";
import axios from "axios";

import Api from "../../../Utils/Api";
import { getUser, getToken } from "../../../Utils/Common";
import { CustomInput } from "../../../Components/CustomInput";
import { CustomAlert } from "../../../Components/CustomAlert";
import { CustomButton } from "../../../Components/CustomButton";
import CustomHeaderText from "../../../Components/CustomHeaderText";

const Create = (props) => {
  let history = useHistory();
  const [kode_prodi, setKodeProdi] = useState();
  const [kode_jurusan, setKodeJurusan] = useState();
  const [nama_jurusan, setNamaJurusan] = useState();
  const [errMsg, setErrMsg] = useState();
  const [showModal, setShowModal] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [validated, setValidated] = useState(false);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    const form = e.currentTarget;
    if (form.checkValidity() === false) {
      e.stopPropagation();
    } else {
      const token = getToken();
      const user = getUser();
      let bodyParameters = {
        kode_prodi,
        kode_jurusan,
        nama_jurusan,
        name: user.name,
      };
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
      };
      await axios
        .post(Api.createJurusan, bodyParameters, config)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            setErrMsg(result.data);
            setShowModal(true);
          } else {
            console.log(result);
            history.goBack();
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }

    setIsLoading(false);
    setValidated(true);
  };

  if (props.isLogin) {
    return (
      <div className="main-container">
        <div className="container">
          <Wrapper>
            <CustomHeaderText title={"Buat Jurusan Baru"} showBack={true} />
            <Form
              noValidate
              validated={validated}
              onSubmit={handleSubmit}
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                width: "40%",
              }}
            >
              <Form.Group as={Col} md="12" controlId="groupKodeProdi">
                <CustomInput
                  controlId="kodeProdiInput"
                  label="Kode Prodi"
                  required={true}
                  placeholder="Kode Prodi"
                  onChange={(e) => setKodeProdi(e.target.value)}
                  feedbackMsg="Kode prodi tidak boleh kosong"
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="groupKodeJurusan">
                <CustomInput
                  controlId="kodeJurusanInput"
                  label="Kode Jurusan"
                  required={true}
                  placeholder="Kode Jurusan"
                  onChange={(e) => setKodeJurusan(e.target.value)}
                  feedbackMsg="Kode jurusan tidak boleh kosong"
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="groupNamaJurusan">
                <CustomInput
                  controlId="namaJurusanInput"
                  label="Nama Jurusan"
                  required={true}
                  placeholder="Nama Jurusan"
                  onChange={(e) => setNamaJurusan(e.target.value)}
                  feedbackMsg="Nama jurusan tidak boleh kosong"
                />
              </Form.Group>
              <CustomButton label={"Log In"} isLoading={isLoading} />
            </Form>
          </Wrapper>
          <CustomAlert
            show={showModal}
            onHide={() => setShowModal(false)}
            modalBody={errMsg}
            onClickYes={() => setShowModal(false)}
          />
        </div>
      </div>
    );
  } else {
    return <Redirect to={"/"} />;
  }
};

export default Create;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;
