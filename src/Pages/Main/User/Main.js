/* eslint-disable */
import React, { useState, useEffect } from "react";
import { Redirect, useHistory } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import PropTypes from "prop-types";
import axios from "axios";

import ApiV2 from "../../../Utils/ApiV2";
import { getUser, getToken } from "../../../Utils/Common";
import { CustomToast } from "../../../Components/CustomToast";
import { CustomLoading } from "../../../Components/CustomLoading";
import { CustomAlert } from "../../../Components/CustomAlert";
import CustomHeaderText from "../../../Components/CustomHeaderText";

const columns = [
  { id: "id", label: "No", minWidth: 90 },
  {
    id: "name",
    label: "Nama",
    minWidth: 100,
  },
  {
    id: "username",
    label: "Username",
    minWidth: 100,
  },
  {
    id: "created_at",
    label: "Waktu",
    minWidth: 100,
  },
];

const useStyles = makeStyles({
  root: {
    width: "100%",
  },
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1,
  },
  container: {
    // maxHeight: 440,
  },
});

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

function EnhancedTableHead(props) {
  const { classes, order, orderBy, onRequestSort } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {columns.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? "right" : "left"}
            padding={headCell.disablePadding ? "none" : "normal"}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : "asc"}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === "desc" ? "sorted descending" : "sorted ascending"}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
        <TableCell padding="checkbox" />
        <TableCell padding="checkbox" />
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(["asc", "desc"]).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

const User = (props) => {
  let history = useHistory();
  const classes = useStyles();
  const [data, setData] = useState([]);
  const [order, setOrder] = useState("asc");
  const [orderBy, setOrderBy] = useState("id");
  const [modalMsg, setModalMsg] = useState("");
  const [activeButton, setActiveButton] = useState("");
  const [idUser, setIDUser] = useState("");
  const [headerText, setHeaderText] = useState("");
  const [bodyText, setBodyText] = useState("");
  const [showToast, setShowToast] = useState(false);
  const [isReady, setIsReady] = useState(false);
  const [action, setAction] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    const token = getToken();
    if (token !== null) {
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
      };
      await axios
        .get(ApiV2.getUser, config)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            console.log("error", result);
          } else {
            setData(result.data);
          }
          setIsReady(true);
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = rows.map((n) => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const handleShowModal = (event, row) => {
    if (event === "Sync") {
      setModalMsg("Lakukan sinkronisasi data Mahasiswa?");
      setActiveButton("Sync");
      setAction(true);
    } else if (event === "Export") {
      setModalMsg("Lakukan Export Data Mahasiswa?");
      setActiveButton("Export");
      setAction(true);
    } else if (event === "Enroll") {
      setModalMsg("Lakukan Enroll Data Mahasiswa?");
      setActiveButton("Enroll");
      setAction(true);
    } else if (event === "Unenroll") {
      setModalMsg("Lakukan Unenroll Data Mahasiswa?");
      setActiveButton("Unenroll");
      setAction(true);
    } else if (event === "DeleteAll") {
      setModalMsg("Lakukan penghapusan data Mahasiswa di dashboard?");
      setActiveButton("DeleteAll");
      setAction(true);
    } else {
      setIDUser(row.id);
      setModalMsg(`Hapus user ${row.name}?`);
      // setAction(false);
    }
    setShowModal(true);
  };

  const handleDel = () => {
    setIsLoading(true);
    const token = getToken();
    const user = getUser();
    if (token !== null) {
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
        data: { id: idUser, name: user.name },
      };
      axios
        .delete(ApiV2.deleteUser, config)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            setShowToast(true);
            setHeaderText("Error");
            setBodyText(result.data);
          } else {
            setShowModal(false);
            setShowToast(true);
            setHeaderText("Success");
            setBodyText(result.data);
            fetchData();
          }
        })
        .catch((err) => {
          alert(err);
          console.log(err);
        })
        .finally(() => setIsLoading(false));
    }
  };

  if (props.isLogin) {
    if (isReady) {
      return (
        <div className="main-container">
          <div className="container">
            <CustomHeaderText title={"Users"} />
            <TableContainer className={classes.container}>
              <Table aria-label="sticky table">
                <EnhancedTableHead
                  classes={classes}
                  order={order}
                  orderBy={orderBy}
                  onSelectAllClick={handleSelectAllClick}
                  onRequestSort={handleRequestSort}
                  rowCount={data.length}
                />
                <TableBody>
                  {stableSort(data, getComparator(order, orderBy))
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row) => {
                      return (
                        <TableRow
                          hover
                          role="checkbox"
                          tabIndex={-1}
                          key={row.id}
                        >
                          {columns.map((column) => {
                            const value = row[column.id];
                            return (
                              <TableCell key={column.id} align={column.align}>
                                {value}
                              </TableCell>
                            );
                          })}
                          <TableCell>
                            <IconButton
                              color="secondary"
                              aria-label="delete"
                              onClick={() => handleShowModal("Delete", row)}
                            >
                              <DeleteIcon />
                            </IconButton>
                          </TableCell>
                          <TableCell>
                            <IconButton
                              color="primary"
                              aria-label="edit"
                              onClick={() =>
                                history.push(`/user/${row.username}/update`)
                              }
                            >
                              <EditIcon />
                            </IconButton>
                          </TableCell>
                        </TableRow>
                      );
                    })}
                </TableBody>
              </Table>
            </TableContainer>
            <TablePagination
              rowsPerPageOptions={[10, 25, 100]}
              component="div"
              count={data.length}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
            />
            <CustomToast
              show={showToast}
              headerText={headerText}
              bodyText={bodyText}
              onClose={() => setShowToast(false)}
            />
            <CustomAlert
              show={showModal}
              isLoading={isLoading}
              onHide={() => {
                setShowModal(false);
                setIDMahasiswa("");
              }}
              modalBody={modalMsg}
              showNoButton
              onClickNo={() => {
                setShowModal(false);
                setIDMahasiswa("");
              }}
              onClickYes={() =>
                action
                  ? activeButton === "DeleteAll"
                    ? handleDelAll()
                    : handleSync()
                  : handleDel()
              }
            />
          </div>
        </div>
      );
    } else {
      return <CustomLoading visible={true} />;
    }
  } else {
    return <Redirect to={"/login"} />;
  }
};

export default User;
