/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import { Redirect, useHistory } from "react-router-dom";
import styled from "styled-components";
import { Form, Col } from "react-bootstrap";
import axios from "axios";

import ApiV2 from "../../../Utils/ApiV2";
import { getUser, getToken } from "../../../Utils/Common";
import { CustomInput } from "../../../Components/CustomInput";
import { CustomAlert } from "../../../Components/CustomAlert";
import { CustomButton } from "../../../Components/CustomButton";
import { CustomLoading } from "../../../Components/CustomLoading";
import CustomHeaderText from "../../../Components/CustomHeaderText";

const Update = (props) => {
  let history = useHistory();
  const [errMsg, setErrMsg] = useState();
  const [detail, setDetail] = useState({});
  const [isReady, setIsReady] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [validated, setValidated] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      const token = getToken();
      if (token !== null) {
        const config = {
          headers: {
            "X-Authorization": token.key,
          },
        };
        await axios
          .get(`${ApiV2.getDetailUser}/${props.match.params.id}`, config)
          .then((res) => {
            const result = res.data;
            if (result.msg === "Error") {
              console.log("error", result);
            } else {
              result.data.password = "";
              setDetail(result.data);
            }
            setIsReady(true);
          })
          .catch((err) => {
            console.log(err);
          });
      }
    };
    fetchData();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    const form = e.currentTarget;
    if (form.checkValidity() === false) {
      e.stopPropagation();
    } else {
      const token = getToken();
      const user = getUser();
      let bodyParameters = {
        name: user.name,
        id: detail.id,
        nama_user: detail.name,
        username: detail.username,
        password: "test",
      };
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
      };
      await axios
        .put(ApiV2.updateUser, bodyParameters, config)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            setErrMsg(result.data);
            setShowModal(true);
          } else {
            history.goBack();
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }
    setIsLoading(false);
    setValidated(true);
  };

  if (props.isLogin) {
    if (isReady) {
      return (
        <div className="main-container">
          <div className="container">
            <Wrapper>
              <CustomHeaderText title={"Update Data User"} showBack={true} />
              <Form
                noValidate
                validated={validated}
                onSubmit={handleSubmit}
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  width: "40%",
                }}
              >
                <Form.Group as={Col} md="12" controlId="groupNamaUser">
                  <CustomInput
                    controlId="namaUserInput"
                    label="Nama User"
                    required={true}
                    placeholder="Nama User"
                    value={detail.name}
                    onChange={(e) =>
                      setDetail({ ...detail, name: e.target.value })
                    }
                    feedbackMsg="Nama user tidak boleh kosong"
                  />
                </Form.Group>
                <Form.Group as={Col} md="12" controlId="groupUsername">
                  <CustomInput
                    controlId="usernameInput"
                    label="Username"
                    required={true}
                    placeholder="Username"
                    value={detail.username}
                    onChange={(e) =>
                      setDetail({ ...detail, username: e.target.value })
                    }
                    feedbackMsg="Username tidak boleh kosong"
                  />
                </Form.Group>
                <Form.Group as={Col} md="12" controlId="groupPassword">
                  <CustomInput
                    controlId="passwordInput"
                    label="Password"
                    required={true}
                    placeholder="Password"
                    value={detail.password}
                    onChange={(e) =>
                      setDetail({ ...detail, password: e.target.value })
                    }
                    feedbackMsg="Password tidak boleh kosong"
                  />
                </Form.Group>
                <CustomButton label={"Submit"} isLoading={isLoading} />
              </Form>
            </Wrapper>
            <CustomAlert
              show={showModal}
              onHide={() => setShowModal(false)}
              modalBody={errMsg}
              onClickYes={() => setShowModal(false)}
            />
          </div>
        </div>
      );
    } else {
      return <CustomLoading visible={true} />;
    }
  } else {
    return <Redirect to={"/"} />;
  }
};

export default Update;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;
