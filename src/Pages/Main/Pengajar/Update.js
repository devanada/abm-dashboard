/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import { Redirect, useHistory } from "react-router-dom";
import styled from "styled-components";
import { Form, Col, Row } from "react-bootstrap";
import axios from "axios";

import Api from "../../../Utils/Api";
import { getUser, getToken } from "../../../Utils/Common";
import { CustomInput } from "../../../Components/CustomInput";
import { CustomAlert } from "../../../Components/CustomAlert";
import { CustomButton } from "../../../Components/CustomButton";
import { CustomLoading } from "../../../Components/CustomLoading";
import CustomHeaderText from "../../../Components/CustomHeaderText";

const Update = (props) => {
  let history = useHistory();
  const [nip, setNIP] = useState();
  const [email, setEmail] = useState();
  const [errMsg, setErrMsg] = useState();
  const [nama_depan, setNamaDepan] = useState();
  const [nama_belakang, setNamaBelakang] = useState();
  const [kode_kelas, setKodeKelas] = useState([{}]);
  const [isReady, setIsReady] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [validated, setValidated] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      const token = getToken();
      if (token !== null) {
        const config = {
          headers: {
            "X-Authorization": token.key,
          },
        };
        await axios
          .post(Api.detailPengajar, { id: props.match.params.id }, config)
          .then((res) => {
            const result = res.data;
            if (result.msg === "Error") {
              console.log("error", result);
            } else {
              const parsed = JSON.parse(result.data.kode_kelas);
              if (parsed) {
                parsed.forEach((o, key) => {
                  o.id = key + 1;
                });
                setKodeKelas(parsed);
              }
              setNamaDepan(result.data.nama_depan);
              setNamaBelakang(result.data.nama_belakang);
              setNIP(result.data.nip);
              setEmail(result.data.email);
            }
            setIsReady(true);
          })
          .catch((err) => {
            console.log(err);
          });
      }
    };
    fetchData();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    const form = e.currentTarget;
    if (form.checkValidity() === false) {
      e.stopPropagation();
    } else {
      var arrayObj1 = [];
      var arrayObj2 = [];
      for (var i = 0; i <= kode_kelas.length - 1; i++) {
        arrayObj1.push(kode_kelas[i].kode);
        arrayObj2.push(kode_kelas[i].migration);
      }
      const token = getToken();
      const user = getUser();
      let bodyParameters = {
        id: props.match.params.id,
        nama_depan,
        nama_belakang,
        nip,
        email,
        kode_kelas: arrayObj1,
        migration: arrayObj2,
        name: user.name,
      };
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
      };
      await axios
        .post(Api.updatePengajar, bodyParameters, config)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            setErrMsg(result.data);
            setShowModal(true);
          } else {
            console.log(result);
            history.goBack();
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }

    setIsLoading(false);
    setValidated(true);
  };

  const addObjToArr = () => {
    const newArr = [];
    newArr.push(...kode_kelas, {
      id: kode_kelas.length + 1,
      kode: "",
      migration: "",
    });
    setKodeKelas(newArr);
  };

  if (props.isLogin) {
    if (isReady) {
      return (
        <div className="main-container">
          <div className="container">
            <Wrapper>
              <CustomHeaderText title={"Update Pengajar"} showBack={true} />
              <Form
                noValidate
                validated={validated}
                onSubmit={handleSubmit}
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  width: "40%",
                }}
              >
                <Form.Group as={Col} md="12" controlId="groupNamaDepan">
                  <CustomInput
                    controlId="namaDepanInput"
                    label="Nama Depan"
                    required={true}
                    placeholder="Nama Depan"
                    value={nama_depan}
                    onChange={(e) => setNamaDepan(e.target.value)}
                    feedbackMsg="Nama depan tidak boleh kosong"
                  />
                </Form.Group>
                <Form.Group as={Col} md="12" controlId="groupNamaBelakang">
                  <CustomInput
                    controlId="namaBelakangInput"
                    label="Nama Belakang"
                    required={true}
                    placeholder="Nama Belakang"
                    value={nama_belakang}
                    onChange={(e) => setNamaBelakang(e.target.value)}
                    feedbackMsg="Nama belakang tidak boleh kosong"
                  />
                </Form.Group>
                <Form.Group as={Col} md="12" controlId="groupNIP">
                  <CustomInput
                    controlId="nipInput"
                    label="NIP"
                    required={true}
                    placeholder="NIP"
                    value={nip}
                    onChange={(e) => setNIP(e.target.value)}
                    feedbackMsg="NIP tidak boleh kosong"
                  />
                </Form.Group>
                <Form.Group as={Col} md="12" controlId="groupEmail">
                  <CustomInput
                    controlId="emailInput"
                    label="Email"
                    required={true}
                    placeholder="Email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    feedbackMsg="Email tidak boleh kosong"
                  />
                </Form.Group>
                <Form.Group as={Col} md="12" controlId="groupKodeKelas">
                  {kode_kelas.map((i, index) => {
                    return (
                      <Row className="g-2" key={index}>
                        <Col md>
                          <CustomInput
                            controlId="kodeKelasInput"
                            label="Kode Kelas"
                            required={true}
                            placeholder="Kode Kelas"
                            value={i.kode}
                            onChange={(e) =>
                              setKodeKelas(
                                kode_kelas.map((el) =>
                                  el.id === i.id
                                    ? { ...el, kode: e.target.value }
                                    : el
                                )
                              )
                            }
                            feedbackMsg="Kode kelas tidak boleh kosong"
                          />
                        </Col>
                        <Col md>
                          <CustomInput
                            controlId="migrationInput"
                            label="Migration"
                            required={true}
                            placeholder="Migration"
                            value={i.migration}
                            onChange={(e) =>
                              setKodeKelas(
                                kode_kelas.map((el) =>
                                  el.id === i.id
                                    ? { ...el, migration: e.target.value }
                                    : el
                                )
                              )
                            }
                            feedbackMsg="Migration tidak boleh kosong"
                          />
                        </Col>
                      </Row>
                    );
                  })}
                  <p
                    style={{ cursor: "pointer", alignSelf: "flex-start" }}
                    onClick={() => addObjToArr()}
                  >
                    + Add Another Payee
                  </p>
                </Form.Group>
                <CustomButton label={"Log In"} isLoading={isLoading} />
              </Form>
            </Wrapper>
            <CustomAlert
              show={showModal}
              onHide={() => setShowModal(false)}
              modalBody={errMsg}
              onClickYes={() => setShowModal(false)}
            />
          </div>
        </div>
      );
    } else {
      return <CustomLoading visible={true} />;
    }
  } else {
    return <Redirect to={"/"} />;
  }
};

export default Update;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;
