/* eslint-disable */
import React, { useState, useEffect } from "react";
import { Redirect, useHistory } from "react-router-dom";
import { Button, ButtonGroup } from "react-bootstrap";
import axios from "axios";

import Api from "../../../Utils/Api";
import ApiV2 from "../../../Utils/ApiV2";
import ApiOcas from "../../../Utils/ApiOcas";
import { getUser, getToken } from "../../../Utils/Common";
import { CustomAlert } from "../../../Components/CustomAlert";
import { CustomToast } from "../../../Components/CustomToast";
import { CustomLoading } from "../../../Components/CustomLoading";
import CustomHeaderText from "../../../Components/CustomHeaderText";
import { CustomTabs } from "../../../Components/CustomTabs";

const columns = [
  { id: "id", label: "No", minWidth: 90 },
  {
    id: "nama_lengkap",
    label: "Nama Lengkap",
    minWidth: 170,
  },
  {
    id: "nik",
    label: "NIK",
    minWidth: 100,
  },
  {
    id: "gelar_awal",
    label: "Gelar Awal",
    minWidth: 100,
  },
  {
    id: "gelar_akhir",
    label: "Gelar Akhir",
    minWidth: 100,
  },
  {
    id: "email",
    label: "Email",
    minWidth: 100,
  },
  {
    id: "hp",
    label: "HP",
    minWidth: 100,
  },
  {
    id: "password",
    label: "Password",
    minWidth: 100,
  },
  {
    id: "migration",
    label: "Migrasi",
    minWidth: 100,
  },
];

const columnsOcas = [
  { id: "id", label: "No", minWidth: 90 },
  {
    id: "nama_lengkap",
    label: "Nama Lengkap",
    minWidth: 170,
  },
  {
    id: "nik",
    label: "NIK",
    minWidth: 100,
  },
  {
    id: "gelar_awal",
    label: "Gelar Awal",
    minWidth: 100,
  },
  {
    id: "gelar_akhir",
    label: "Gelar Akhir",
    minWidth: 100,
  },
  {
    id: "email",
    label: "Email",
    minWidth: 100,
  },
  {
    id: "hp",
    label: "HP",
    minWidth: 100,
  },
  {
    id: "password",
    label: "Password",
    minWidth: 100,
  },
];

const Main = (props) => {
  let history = useHistory();
  const [data, setData] = useState([]);
  const [ocas, setOcas] = useState([]);
  const [modalMsg, setModalMsg] = useState("");
  const [idPengajar, setIDPengajar] = useState("");
  const [activeButton, setActiveButton] = useState("");
  const [action, setAction] = useState(false);
  const [isReady, setIsReady] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [showToast, setShowToast] = useState(false);
  const [headerText, setHeaderText] = useState("");
  const [bodyText, setBodyText] = useState("");

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    setIsReady(false);
    const token = getToken();
    if (token !== null) {
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
      };
      await axios
        .get(ApiV2.listDosen, config)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            console.log("error", result);
          } else {
            result.data.forEach((o, key) => {
              result.data[key].id = key + 1;
            });
            setData(result.data);
          }
        })
        .catch((err) => {
          console.log(err);
        });
      await axios
        .get(ApiOcas.listTeachers)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            console.log("error", result);
          } else {
            result.data.forEach((o, key) => {
              result.data[key].id = key + 1;
            });
            setOcas(result.data);
          }
        })
        .catch((err) => {
          console.log(err);
        });
      setIsReady(true);
    }
  };

  const handleShowModal = (event, row) => {
    if (event === "Sync") {
      setModalMsg("Lakukan sinkronisasi data Pengajar?");
      setActiveButton("Sync");
      setAction(true);
    } else if (event === "Export") {
      setModalMsg("Lakukan export data Pengajar?");
      setActiveButton("Export");
      setAction(true);
    } else if (event === "DeleteAll") {
      setModalMsg("Lakukan penghapusan data Pengajar di dashboard?");
      setActiveButton("DeleteAll");
      setAction(true);
    } else {
      setIDPengajar(row.nik);
      setModalMsg(`Hapus Pengajar ${row.nama_lengkap}?`);
      setAction(false);
    }
    setShowModal(true);
  };

  const handleDel = () => {
    setIsLoading(true);
    const token = getToken();
    const user = getUser();
    if (token !== null) {
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
        data: { nik: idPengajar, name: user.name },
      };
      axios
        .delete(ApiV2.deleteDosen, config)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            setShowToast(true);
            setHeaderText("Error");
            setBodyText(result.data);
          } else {
            setShowModal(false);
            setShowToast(true);
            setHeaderText("Success");
            setBodyText(result.data);
            fetchData();
          }
        })
        .catch((err) => {
          console.log(err);
        })
        .finally(() => setIsLoading(false));
    }
  };

  const handleDelAll = () => {
    setIsLoading(true);
    const token = getToken();
    const user = getUser();
    if (token !== null) {
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
        data: {
          name: user.name,
        },
      };
      axios
        .delete(ApiV2.deleteAllDosen, config)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            setShowToast(true);
            setHeaderText("Error");
            setBodyText(result.data);
            console.log("error", result);
          } else {
            setShowModal(false);
            setShowToast(true);
            setHeaderText("Success");
            setBodyText(result.data);
            fetchData();
          }
        })
        .catch((err) => {
          console.log(err);
        })
        .finally(() => setIsLoading(false));
    }
  };

  const handleSync = () => {
    setIsLoading(true);
    const token = getToken();
    const user = getUser();
    if (token !== null) {
      let url;
      if (activeButton === "Sync") {
        url = ApiV2.syncDosen;
      } else {
        url = ApiV2.exportDosen;
      }
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
      };
      axios
        .post(url, { name: user.name }, config)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            alert(result.data);
            console.log("error", result);
          } else {
            setShowModal(false);
            setShowToast(true);
            setHeaderText("Success");
            setBodyText(result.data);
            fetchData();
          }
        })
        .catch((err) => {
          console.log(err);
        })
        .finally(() => setIsLoading(false));
    }
  };

  const handleMigrateOne = (e, whichButton) => {
    setIsLoading(true);
    let url;
    if (whichButton === "Ocas") {
      url = ApiV2.syncDosenOne;
    } else {
      url = ApiV2.exportDosenOne;
    }
    const token = getToken();
    const user = getUser();
    if (token !== null) {
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
      };
      axios
        .post(url, { nik: e.nik, name: user.name }, config)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            console.log("error", result);
          } else {
            setShowToast(true);
            setHeaderText("Success");
            setBodyText(result.data);
            fetchData();
          }
        })
        .catch((err) => {
          console.log(err);
        })
        .finally(() => setIsLoading(false));
    }
  };

  if (props.isLogin) {
    if (isReady) {
      return (
        <div className="main-container">
          <div className="container">
            <CustomHeaderText title={"Pengajar"} />
            <div className="mb-3">
              <ButtonGroup className="me-2" aria-label="Basic example">
                <Button
                  variant={"primary"}
                  onClick={() => handleShowModal("Sync")}
                >
                  Sync Data
                </Button>
              </ButtonGroup>
              <ButtonGroup className="me-2" aria-label="Basic example">
                <Button
                  variant={"primary"}
                  onClick={() => handleShowModal("Export")}
                >
                  Export Data
                </Button>
              </ButtonGroup>
              <ButtonGroup className="me-2" aria-label="Basic example">
                <Button
                  variant={"primary"}
                  onClick={() => handleShowModal("DeleteAll")}
                >
                  Delete All Data
                </Button>
              </ButtonGroup>
            </div>
            <CustomTabs
              columnsOcas={columnsOcas}
              columns={columns}
              ocas={ocas}
              data={data}
              onClick1={(e) => handleShowModal("Delete", e)}
              // onClick2={(e) => history.push(`/pengajar/${e.nik}/update`)}
              // onClick3={(e) => handleMigrateOne(e, "Ocas")}
              onClick4={(e) => handleMigrateOne(e, "Dashboard")}
            />
            <CustomToast
              show={showToast}
              headerText={headerText}
              bodyText={bodyText}
              onClose={() => setShowToast(false)}
            />
            <CustomAlert
              show={showModal}
              isLoading={isLoading}
              onHide={() => {
                setShowModal(false);
                setIDPengajar("");
              }}
              modalBody={modalMsg}
              showNoButton
              onClickNo={() => {
                setShowModal(false);
                setIDPengajar("");
              }}
              onClickYes={() =>
                action
                  ? activeButton === "DeleteAll"
                    ? handleDelAll()
                    : handleSync()
                  : handleDel()
              }
            />
          </div>
        </div>
      );
    } else {
      return <CustomLoading visible={true} />;
    }
  } else {
    return <Redirect to={"/login"} />;
  }
};

export default Main;
