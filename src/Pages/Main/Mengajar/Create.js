import React, { useState } from "react";
import { Redirect, useHistory } from "react-router-dom";
import styled from "styled-components";
import { Form, Col } from "react-bootstrap";
import axios from "axios";

import Api from "../../../Utils/Api";
import { getUser, getToken } from "../../../Utils/Common";
import { CustomInput } from "../../../Components/CustomInput";
import { CustomAlert } from "../../../Components/CustomAlert";
import { CustomButton } from "../../../Components/CustomButton";
import CustomHeaderText from "../../../Components/CustomHeaderText";

const Create = (props) => {
  let history = useHistory();
  const [nip, setNIP] = useState();
  const [email, setEmail] = useState();
  const [errMsg, setErrMsg] = useState();
  const [nama_depan, setNamaDepan] = useState();
  const [nama_belakang, setNamaBelakang] = useState();
  const [kode_kelas, setKodeKelas] = useState([{}]);
  const [showModal, setShowModal] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [validated, setValidated] = useState(false);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    const form = e.currentTarget;
    if (form.checkValidity() === false) {
      e.stopPropagation();
    } else {
      var arrayObj = [];
      for (var i = 0; i <= kode_kelas.length - 1; i++) {
        arrayObj.push(kode_kelas[i].name);
      }
      const token = getToken();
      const user = getUser();
      let bodyParameters = {
        nama_depan,
        nama_belakang,
        nip,
        email,
        kode_kelas: arrayObj,
        name: user.name,
      };
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
      };
      await axios
        .post(Api.createPengajar, bodyParameters, config)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            setErrMsg(result.data);
            setShowModal(true);
          } else {
            console.log(result);
            history.goBack();
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }

    setIsLoading(false);
    setValidated(true);
  };

  const addObjToArr = () => {
    const newArr = [];
    newArr.push(...kode_kelas, {});
    setKodeKelas(newArr);
  };

  if (props.isLogin) {
    return (
      <div className="main-container">
        <div className="container">
          <Wrapper>
            <CustomHeaderText title={"Buat Pengajar Baru"} showBack={true} />
            <Form
              noValidate
              validated={validated}
              onSubmit={handleSubmit}
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                width: "40%",
              }}
            >
              <Form.Group as={Col} md="12" controlId="groupNamaDepan">
                <CustomInput
                  controlId="namaDepanInput"
                  label="Nama Depan"
                  required={true}
                  placeholder="Nama Depan"
                  onChange={(e) => setNamaDepan(e.target.value)}
                  feedbackMsg="Nama depan tidak boleh kosong"
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="groupNamaBelakang">
                <CustomInput
                  controlId="namaBelakangInput"
                  label="Nama Belakang"
                  required={true}
                  placeholder="Nama Belakang"
                  onChange={(e) => setNamaBelakang(e.target.value)}
                  feedbackMsg="Nama belakang tidak boleh kosong"
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="groupNIP">
                <CustomInput
                  controlId="nipInput"
                  label="NIP"
                  required={true}
                  placeholder="NIP"
                  onChange={(e) => setNIP(e.target.value)}
                  feedbackMsg="NIP tidak boleh kosong"
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="groupEmail">
                <CustomInput
                  controlId="emailInput"
                  label="Email"
                  required={true}
                  placeholder="Email"
                  onChange={(e) => setEmail(e.target.value)}
                  feedbackMsg="Email tidak boleh kosong"
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="groupKodeKelas">
                {kode_kelas.map((i, index) => {
                  return (
                    <Col key={index}>
                      <CustomInput
                        controlId="kodeKelasInput"
                        label="Kode Kelas"
                        required={true}
                        placeholder="Kode Kelas"
                        onChange={(e) => (i.name = e.target.value)}
                        feedbackMsg="Kode kelas tidak boleh kosong"
                      />
                    </Col>
                  );
                })}
                <p
                  style={{ cursor: "pointer", alignSelf: "flex-start" }}
                  onClick={() => addObjToArr()}
                >
                  + Add Another Payee
                </p>
              </Form.Group>
              <CustomButton label={"Log In"} isLoading={isLoading} />
            </Form>
          </Wrapper>
          <CustomAlert
            show={showModal}
            onHide={() => setShowModal(false)}
            modalBody={errMsg}
            onClickYes={() => setShowModal(false)}
          />
        </div>
      </div>
    );
  } else {
    return <Redirect to={"/"} />;
  }
};

export default Create;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;
