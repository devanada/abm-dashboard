/* eslint-disable */
import React, { useState, useEffect, useCallback } from "react";
import { Redirect, useHistory } from "react-router-dom";
import { Button, ButtonGroup, Form } from "react-bootstrap";
import axios from "axios";

import Api from "../../../Utils/Api";
import ApiV2 from "../../../Utils/ApiV2";
import ApiOcas from "../../../Utils/ApiOcas";
import { getUser, getToken } from "../../../Utils/Common";
import { CustomAlert } from "../../../Components/CustomAlert";
import { CustomToast } from "../../../Components/CustomToast";
import { CustomLoading } from "../../../Components/CustomLoading";
import CustomHeaderText from "../../../Components/CustomHeaderText";
import { CustomTabs } from "../../../Components/CustomTabs";

const columns = [
  { id: "id", label: "No", minWidth: 90 },
  {
    id: "nama_mata_kuliah",
    label: "Nama Mata Kuliah",
    minWidth: 170,
  },
  {
    id: "kelas_id",
    label: "Kelas ID",
    minWidth: 100,
  },
  {
    id: "program_id",
    label: "Program ID",
    minWidth: 100,
  },
  {
    id: "nik",
    label: "NIK",
    minWidth: 100,
  },
  {
    id: "nama_kelas",
    label: "Nama Kelas",
    minWidth: 100,
  },
  {
    id: "enroll",
    label: "Enroll",
    minWidth: 100,
  },
];

const columnsOcas = [
  { id: "id", label: "No", minWidth: 90 },
  {
    id: "nama_mata_kuliah",
    label: "Nama Mata Kuliah",
    minWidth: 170,
  },
  {
    id: "kelas_id",
    label: "Kelas ID",
    minWidth: 100,
  },
  {
    id: "program_id",
    label: "Program ID",
    minWidth: 100,
  },
  {
    id: "nik",
    label: "NIK",
    minWidth: 100,
  },
];

const Main = (props) => {
  let history = useHistory();
  const [, updateState] = useState();
  const forceUpdate = useCallback(() => updateState({}), []);
  const [data, setData] = useState([]);
  const [ocas, setOcas] = useState([]);
  const [pengajar, setPengajar] = useState([]);
  const [modalMsg, setModalMsg] = useState("");
  const [idPengajar, setIDPengajar] = useState("");
  const [activeButton, setActiveButton] = useState("");
  const [selectedPengajar, setSelectedPengajar] = useState("");
  const [action, setAction] = useState(false);
  const [isReady, setIsReady] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [showToast, setShowToast] = useState(false);
  const [headerText, setHeaderText] = useState("");
  const [bodyText, setBodyText] = useState("");

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    setIsReady(false);
    const token = getToken();
    if (token !== null) {
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
      };
      await axios
        .get(ApiOcas.listTeachers)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            console.log("error", result);
          } else {
            setPengajar(result.data);
          }
        })
        .catch((err) => {
          console.log(err);
        });
      setIsReady(true);
    }
  };

  const handleShowModal = (event, row) => {
    if (event === "Sync") {
      setModalMsg("Lakukan sinkronisasi data mengajar?");
      setActiveButton("Sync");
      setAction(true);
    } else if (event === "Enroll") {
      setModalMsg("Lakukan enroll data Pengajar?");
      setActiveButton("Enroll");
      setAction(true);
    } else if (event === "Unenroll") {
      setModalMsg("Lakukan unenroll data Pengajar?");
      setActiveButton("Unenroll");
      setAction(true);
    } else {
      setIDPengajar(row.nik);
      setModalMsg(`Hapus Pengajar ${row.nama_lengkap}?`);
      setAction(false);
    }
    setShowModal(true);
  };

  const handleMengajarOcas = (e) => {
    setIsLoading(true);
    const token = getToken();
    const user = getUser();
    if (token !== null) {
      axios
        .get(ApiOcas.teachings)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            console.log("error", result);
          } else {
            result.data[e].forEach((o, key) => {
              result.data[e][key].id = key + 1;
            });
            setOcas(result.data[e]);
            setIsLoading(false);
          }
        })
        .catch((err) => {
          console.log(err);
        })
        .finally(() => fetchData());
    }
  };

  const handleMengajar = (e) => {
    setIsLoading(true);
    const token = getToken();
    const user = getUser();
    if (token !== null) {
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
      };
      axios
        .post(ApiV2.getAllMengajarDosen, { nik: e, name: user.name }, config)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            console.log("error", result);
          } else {
            result.data.forEach((o, key) => {
              result.data[key].id = key + 1;
            });
            console.log(result.data);
            handleMengajarOcas(e);
            setData(result.data);
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  const handleDel = () => {
    setIsLoading(true);
    const token = getToken();
    const user = getUser();
    if (token !== null) {
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
        data: { nik: idPengajar, name: user.name },
      };
      axios
        .delete(ApiV2.deleteDosen, config)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            setShowToast(true);
            setHeaderText("Error");
            setBodyText(result.data);
          } else {
            setShowModal(false);
            setShowToast(true);
            setHeaderText("Success");
            setBodyText(result.data);
            fetchData();
          }
        })
        .catch((err) => {
          console.log(err);
        })
        .finally(() => setIsLoading(false));
    }
  };

  const handleDelAll = () => {
    setIsLoading(true);
    const token = getToken();
    const user = getUser();
    if (token !== null) {
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
        data: {
          name: user.name,
        },
      };
      axios
        .delete(ApiV2.deleteAllDosen, config)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            setShowToast(true);
            setHeaderText("Error");
            setBodyText(result.data);
            console.log("error", result);
          } else {
            setShowModal(false);
            setShowToast(true);
            setHeaderText("Success");
            setBodyText(result.data);
            fetchData();
          }
        })
        .catch((err) => {
          console.log(err);
        })
        .finally(() => setIsLoading(false));
    }
  };

  const handleSync = () => {
    setIsLoading(true);
    const token = getToken();
    const user = getUser();
    if (token !== null) {
      let url;
      if (activeButton === "Sync") {
        url = ApiV2.syncAllMengajarDosen;
      } else if (activeButton === "Enroll") {
        url = ApiV2.enrollDosen;
      } else if (activeButton === "Unenroll") {
        url = ApiV2.unenrollDosen;
      } else {
        url = ApiV2.exportDosen;
      }
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
        data: { name: user.name, nik: selectedPengajar },
      };
      axios
        .post(url, { name: user.name }, config)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            alert(result.data);
            console.log("error", result);
          } else {
            setShowModal(false);
            setShowToast(true);
            setHeaderText("Success");
            setBodyText(result.data);
            fetchData();
          }
        })
        .catch((err) => {
          console.log(err);
        })
        .finally(() => setIsLoading(false));
    }
  };

  const handleMigrateOne = (e, whichButton) => {
    setIsLoading(true);
    let url;
    if (whichButton === "Ocas") {
      url = ApiV2.syncDosenOne;
    } else {
      url = ApiV2.exportDosenOne;
    }
    const token = getToken();
    const user = getUser();
    if (token !== null) {
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
      };
      axios
        .post(url, { nik: e.nik, name: user.name }, config)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            console.log("error", result);
          } else {
            setShowToast(true);
            setHeaderText("Success");
            setBodyText(result.data);
            fetchData();
          }
        })
        .catch((err) => {
          console.log(err);
        })
        .finally(() => setIsLoading(false));
    }
  };

  const handleUnenrollOne = (e) => {
    setIsLoading(true);
    let url;
    if (e.enroll === "sudah") {
      url = ApiV2.unenrollDosenOne;
    } else {
      url = ApiV2.enrollDosenOne;
    }
    const token = getToken();
    const user = getUser();
    if (token !== null) {
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
      };
      axios
        .post(
          url,
          {
            nik: e.nik,
            name: user.name,
            nama_mata_kuliah: e.nama_mata_kuliah,
            kelas_id: e.kelas_id,
          },
          config
        )
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            setShowToast(true);
            setHeaderText("Error");
            setBodyText(result.data);
          } else {
            setShowToast(true);
            setHeaderText("Success");
            setBodyText(result.data);
            fetchData();
          }
        })
        .catch((err) => {
          alert(err);
          console.log(err);
        })
        .finally(() => setIsLoading(false));
    }
  };

  if (props.isLogin) {
    if (isReady) {
      return (
        <div className="main-container">
          <div className="container">
            <CustomHeaderText title={"Data Mengajar"} />
            <div className="mb-3">
              <ButtonGroup className="me-2" aria-label="Basic example">
                <Button
                  variant={"primary"}
                  onClick={() => handleShowModal("Sync")}
                >
                  Sync Data
                </Button>
              </ButtonGroup>
              <ButtonGroup className="me-2" aria-label="Basic example">
                <Button
                  variant={"primary"}
                  onClick={() => handleShowModal("Enroll")}
                >
                  Enroll All
                </Button>
              </ButtonGroup>
              <ButtonGroup className="me-2" aria-label="Basic example">
                <Button
                  variant={"primary"}
                  onClick={() => handleShowModal("Unenroll")}
                >
                  Unenroll All
                </Button>
              </ButtonGroup>
              <ButtonGroup className="me-2" aria-label="Basic example">
                <Form.Select
                  aria-label="Pilih Pengajar"
                  value={selectedPengajar}
                  onChange={(e) => {
                    handleMengajar(e.target.value);
                    setSelectedPengajar(e.target.value);
                  }}
                >
                  <option>Pilih Pengajar</option>
                  {pengajar.map((item, index) => (
                    <option value={item.nik} key={item.nama_lengkap}>
                      {item.gelar_awal} {item.nama_lengkap} {item.gelar_akhir}
                    </option>
                  ))}
                </Form.Select>
              </ButtonGroup>
            </div>
            <CustomTabs
              columnsOcas={columnsOcas}
              columns={columns}
              ocas={ocas}
              data={data}
              // onClick1={(e) => console.log()}
              // onClick2={(e) => history.push(`/pengajar/${e.nik}/update`)}
              // onClick3={(e) => console.log()}
              onClick5={(e) => handleUnenrollOne(e)}
            />
            <CustomToast
              show={showToast}
              headerText={headerText}
              bodyText={bodyText}
              onClose={() => setShowToast(false)}
            />
            <CustomAlert
              show={showModal}
              isLoading={isLoading}
              onHide={() => {
                setShowModal(false);
                setIDPengajar("");
              }}
              modalBody={modalMsg}
              showNoButton
              onClickNo={() => {
                setShowModal(false);
                setIDPengajar("");
              }}
              onClickYes={() =>
                action
                  ? activeButton === "DeleteAll"
                    ? handleDelAll()
                    : handleSync()
                  : handleDel()
              }
            />
          </div>
        </div>
      );
    } else {
      return <CustomLoading visible={true} />;
    }
  } else {
    return <Redirect to={"/login"} />;
  }
};

export default Main;
