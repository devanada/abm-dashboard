/* eslint-disable */
import React, { useState, useEffect } from "react";
import { Redirect, useHistory } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import IconButton from "@mui/material/IconButton";
import TextField from "@mui/material/TextField";
import ClearIcon from "@mui/icons-material/Clear";
import SearchIcon from "@mui/icons-material/Search";
import PropTypes from "prop-types";
import axios from "axios";
import DatePicker from "react-datepicker";
import moment from "moment";

import "react-datepicker/dist/react-datepicker.css";

import ApiV2 from "../../Utils/ApiV2";
import { getToken } from "../../Utils/Common";
import { CustomLoading } from "../../Components/CustomLoading";
import CustomHeaderText from "../../Components/CustomHeaderText";

const columns = [
  { id: "id", label: "No", minWidth: 90 },
  {
    id: "username",
    label: "Username",
    minWidth: 100,
  },
  {
    id: "email",
    label: "Email",
    minWidth: 100,
  },
  {
    id: "meeting_id",
    label: "Meeting ID",
    minWidth: 100,
  },
  {
    id: "topic",
    label: "Topic",
    minWidth: 150,
  },
  {
    id: "date_meeting",
    label: "Tanggal Meeting",
    minWidth: 100,
  },
  {
    id: "start_meeting",
    label: "Start Meeting",
    minWidth: 130,
  },
  {
    id: "end_meeting",
    label: "End Meeting",
    minWidth: 130,
  },
  {
    id: "join_time",
    label: "Waktu Join",
    minWidth: 130,
  },
  {
    id: "leave_time",
    label: "Waktu Keluar",
    minWidth: 130,
  },
  {
    id: "leave_reason",
    label: "Alasan Keluar",
    minWidth: 200,
  },
  {
    id: "duration_join",
    label: "Durasi Join",
    minWidth: 100,
  },
  {
    id: "keterangan",
    label: "Keterangan",
    minWidth: 100,
  },
];

function escapeRegExp(value) {
  return value.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
}

const useStyles = makeStyles({
  root: {
    width: "100%",
  },
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1,
  },
  container: {
    // maxHeight: 440,
  },
});

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

function EnhancedTableHead(props) {
  const { classes, order, orderBy, onRequestSort } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {columns.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? "right" : "left"}
            padding={headCell.disablePadding ? "none" : "normal"}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : "asc"}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === "desc" ? "sorted descending" : "sorted ascending"}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(["asc", "desc"]).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

const Zoom = (props) => {
  let history = useHistory();
  const classes = useStyles();
  const [data, setData] = useState([]);
  const [datas, setDatas] = useState([]);
  const [order, setOrder] = useState("asc");
  const [orderBy, setOrderBy] = useState("id");
  const [isReady, setIsReady] = useState(false);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [searchText, setSearchText] = useState("");
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());

  useEffect(() => {
    setIsReady(true);
    // fetchData();
  }, []);

  const fetchData = async () => {
    const token = getToken();
    if (token !== null) {
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
      };
      await axios
        .get(ApiV2.zoomActivity, config)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            console.log("error", result);
          } else {
            result.data.forEach((o, key) => {
              result.data[key].id = key + 1;
            });
            setData(result.data);
            setDatas(result.data);
          }
        })
        .catch((err) => {
          console.log(err);
        })
        .finally(() => setIsReady(true));
    }
  };

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = rows.map((n) => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const requestSearch = (searchValue) => {
    setSearchText(searchValue);
    const searchRegex = new RegExp(escapeRegExp(searchValue), "i");
    const filteredRows = datas.filter((row) => {
      return Object.keys(row).some((field) => {
        return searchRegex.test(row[field] ? row[field].toString() : null);
      });
    });
    setData(filteredRows);
  };

  const handleDate = (date, event) => {
    if (event === "start") {
      console.log(moment(date).format("D-M-YYYY"));
      setStartDate(date);
    } else {
      setEndDate(date);
    }
  };

  if (props.isLogin) {
    if (isReady) {
      return (
        <div className="main-container">
          <div className="container">
            <CustomHeaderText title={"Aktivitas Zoom"} />
            <TableContainer className={classes.container}>
              <TextField
                variant="standard"
                value={searchText}
                onChange={(e) => requestSearch(e.target.value)}
                placeholder="Search…"
                InputProps={{
                  startAdornment: <SearchIcon fontSize="small" />,
                  endAdornment: (
                    <IconButton
                      title="Clear"
                      aria-label="Clear"
                      size="small"
                      style={{
                        visibility: searchText ? "visible" : "hidden",
                      }}
                      onClick={() => requestSearch("")}
                    >
                      <ClearIcon fontSize="small" />
                    </IconButton>
                  ),
                }}
                sx={{
                  width: {
                    xs: 1,
                    sm: "auto",
                  },
                  m: (theme) => theme.spacing(1, 0.5, 1.5),
                  "& .MuiSvgIcon-root": {
                    mr: 0.5,
                  },
                  "& .MuiInput-underline:before": {
                    borderBottom: 1,
                    borderColor: "divider",
                  },
                }}
              />
              <DatePicker
                selected={startDate}
                onChange={(date) => handleDate(date, "start")}
              />
              <DatePicker
                selected={endDate}
                onChange={(date) => handleDate(date, "end")}
              />
              <Table aria-label="sticky table">
                <EnhancedTableHead
                  classes={classes}
                  order={order}
                  orderBy={orderBy}
                  onSelectAllClick={handleSelectAllClick}
                  onRequestSort={handleRequestSort}
                  rowCount={data.length}
                />
                <TableBody>
                  {stableSort(data, getComparator(order, orderBy))
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((row) => {
                      return (
                        <TableRow
                          hover
                          role="checkbox"
                          tabIndex={-1}
                          key={row.id}
                        >
                          {columns.map((column) => {
                            const value = row[column.id];
                            return (
                              <TableCell key={column.id} align={column.align}>
                                {value ? value : "-"}
                              </TableCell>
                            );
                          })}
                        </TableRow>
                      );
                    })}
                </TableBody>
              </Table>
            </TableContainer>
            <TablePagination
              rowsPerPageOptions={[10, 25, 100]}
              component="div"
              count={data.length}
              rowsPerPage={rowsPerPage}
              page={page}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
            />
          </div>
        </div>
      );
    } else {
      return <CustomLoading visible={true} />;
    }
  } else {
    return <Redirect to={"/login"} />;
  }
};

export default Zoom;
