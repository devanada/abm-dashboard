import React, { useState } from "react";
import { Redirect, useHistory } from "react-router-dom";
import styled from "styled-components";
import { Form, Col } from "react-bootstrap";
import axios from "axios";

import Api from "../../../Utils/Api";
import { getUser, getToken } from "../../../Utils/Common";
import { CustomInput } from "../../../Components/CustomInput";
import { CustomAlert } from "../../../Components/CustomAlert";
import { CustomButton } from "../../../Components/CustomButton";
import CustomHeaderText from "../../../Components/CustomHeaderText";

const Create = (props) => {
  let history = useHistory();
  const [errMsg, setErrMsg] = useState();
  const [kode_prodi, setKodeProdi] = useState();
  const [nama_prodi, setNamaProdi] = useState();
  const [kode_semester, setKodeSemester] = useState();
  const [showModal, setShowModal] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [validated, setValidated] = useState(false);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    const form = e.currentTarget;
    if (form.checkValidity() === false) {
      e.stopPropagation();
    } else {
      const token = getToken();
      const user = getUser();
      let bodyParameters = {
        kode_semester,
        kode_prodi,
        nama_prodi,
        name: user.name,
      };
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
      };
      await axios
        .post(Api.createProdi, bodyParameters, config)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            setErrMsg(result.data);
            setShowModal(true);
          } else {
            console.log(result);
            history.goBack();
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }

    setIsLoading(false);
    setValidated(true);
  };

  if (props.isLogin) {
    return (
      <div className="main-container">
        <div className="container">
          <Wrapper>
            <CustomHeaderText title={"Buat Prodi Baru"} showBack={true} />
            <Form
              noValidate
              validated={validated}
              onSubmit={handleSubmit}
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                width: "40%",
              }}
            >
              <Form.Group as={Col} md="12" controlId="groupKodeSemester">
                <CustomInput
                  controlId="kodeSemesterInput"
                  label="Kode Semester"
                  required={true}
                  placeholder="Kode Semester"
                  onChange={(e) => setKodeSemester(e.target.value)}
                  feedbackMsg="Kode semester tidak boleh kosong"
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="groupKodeProdi">
                <CustomInput
                  controlId="kodeProdiInput"
                  label="Kode Prodi"
                  required={true}
                  placeholder="Kode Prodi"
                  onChange={(e) => setKodeProdi(e.target.value)}
                  feedbackMsg="Kode prodi tidak boleh kosong"
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="groupNamaProdi">
                <CustomInput
                  controlId="namaProdiInput"
                  label="Nama Prodi"
                  required={true}
                  placeholder="Nama Prodi"
                  onChange={(e) => setNamaProdi(e.target.value)}
                  feedbackMsg="Nama prodi tidak boleh kosong"
                />
              </Form.Group>
              <CustomButton label={"Log In"} isLoading={isLoading} />
            </Form>
          </Wrapper>
          <CustomAlert
            show={showModal}
            onHide={() => setShowModal(false)}
            modalBody={errMsg}
            onClickYes={() => setShowModal(false)}
          />
        </div>
      </div>
    );
  } else {
    return <Redirect to={"/"} />;
  }
};

export default Create;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;
