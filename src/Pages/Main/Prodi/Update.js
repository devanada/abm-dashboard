/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import { Redirect, useHistory } from "react-router-dom";
import styled from "styled-components";
import { Form, Col } from "react-bootstrap";
import axios from "axios";

import Api from "../../../Utils/Api";
import { getUser, getToken } from "../../../Utils/Common";
import { CustomInput } from "../../../Components/CustomInput";
import { CustomAlert } from "../../../Components/CustomAlert";
import { CustomButton } from "../../../Components/CustomButton";
import { CustomLoading } from "../../../Components/CustomLoading";
import CustomHeaderText from "../../../Components/CustomHeaderText";

const Update = (props) => {
  let history = useHistory();
  const [errMsg, setErrMsg] = useState();
  const [kode_prodi, setKodeProdi] = useState();
  const [nama_prodi, setNamaProdi] = useState();
  const [kode_semester, setKodeSemester] = useState();
  const [isReady, setIsReady] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [validated, setValidated] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      const token = getToken();
      if (token !== null) {
        const config = {
          headers: {
            "X-Authorization": token.key,
          },
        };
        await axios
          .post(Api.detailProdi, { id: props.match.params.id }, config)
          .then((res) => {
            const result = res.data;
            if (result.msg === "Error") {
              console.log("error", result);
            } else {
              setKodeSemester(result.data.kode_semester);
              setKodeProdi(result.data.kode_prodi);
              setNamaProdi(result.data.nama_prodi);
            }
            setIsReady(true);
          })
          .catch((err) => {
            console.log(err);
          });
      }
    };
    fetchData();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    const form = e.currentTarget;
    if (form.checkValidity() === false) {
      e.stopPropagation();
    } else {
      const token = getToken();
      const user = getUser();
      let bodyParameters = {
        id: props.match.params.id,
        kode_semester,
        kode_prodi,
        nama_prodi,
        name: user.name,
      };
      const config = {
        headers: {
          "X-Authorization": token.key,
        },
      };
      await axios
        .post(Api.updateProdi, bodyParameters, config)
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            setErrMsg(result.data);
            setShowModal(true);
          } else {
            console.log(result);
            history.goBack();
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }

    setIsLoading(false);
    setValidated(true);
  };

  if (props.isLogin) {
    if (isReady) {
      return (
        <div className="main-container">
          <div className="container">
            <Wrapper>
              <CustomHeaderText title={"Update Prodi"} showBack={true} />
              <Form
                noValidate
                validated={validated}
                onSubmit={handleSubmit}
                style={{
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  width: "40%",
                }}
              >
                <Form.Group as={Col} md="12" controlId="groupKodeSemester">
                  <CustomInput
                    controlId="kodeSemesterInput"
                    label="Kode Semester"
                    required={true}
                    placeholder="Kode Semester"
                    value={kode_semester}
                    onChange={(e) => setKodeSemester(e.target.value)}
                    feedbackMsg="Kode semester tidak boleh kosong"
                  />
                </Form.Group>
                <Form.Group as={Col} md="12" controlId="groupKodeProdi">
                  <CustomInput
                    controlId="kodeProdiInput"
                    label="Kode Prodi"
                    required={true}
                    placeholder="Kode Prodi"
                    value={kode_prodi}
                    onChange={(e) => setKodeProdi(e.target.value)}
                    feedbackMsg="Kode prodi tidak boleh kosong"
                  />
                </Form.Group>
                <Form.Group as={Col} md="12" controlId="groupNamaProdi">
                  <CustomInput
                    controlId="namaProdiInput"
                    label="Nama Prodi"
                    required={true}
                    placeholder="Nama Prodi"
                    value={nama_prodi}
                    onChange={(e) => setNamaProdi(e.target.value)}
                    feedbackMsg="Nama prodi tidak boleh kosong"
                  />
                </Form.Group>
                <CustomButton label={"Log In"} isLoading={isLoading} />
              </Form>
            </Wrapper>
            <CustomAlert
              show={showModal}
              onHide={() => setShowModal(false)}
              modalBody={errMsg}
              onClickYes={() => setShowModal(false)}
            />
          </div>
        </div>
      );
    } else {
      return <CustomLoading visible={true} />;
    }
  } else {
    return <Redirect to={"/"} />;
  }
};

export default Update;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;
