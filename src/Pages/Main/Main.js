/* eslint-disable */
import React, { useState, useEffect } from "react";
import { Redirect, useHistory } from "react-router-dom";

// import Api from "../Utils/Api";
import { getUser, getToken } from "../../Utils/Common";
import { CustomLoading } from "../../Components/CustomLoading";

const Main = (props) => {
  if (props.isLogin) {
    return (
      <div className="main-container">
        <div className="container">
          <p>Main Page Dashboard</p>
        </div>
      </div>
    );
  } else {
    return <Redirect to={"/login"} />;
  }
};

export default Main;
