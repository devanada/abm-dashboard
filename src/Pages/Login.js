import React, { useState } from "react";
import { Redirect } from "react-router-dom";
import styled from "styled-components";
import { Form, Col } from "react-bootstrap";
import axios from "axios";

import ApiV2 from "../Utils/ApiV2";
import { CustomInput } from "../Components/CustomInput";
import { CustomButton } from "../Components/CustomButton";

const Login = (props) => {
  const [username, setUserName] = useState();
  const [password, setPassword] = useState();
  const [errMsg, setErrMsg] = useState();
  const [isError, setIsError] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [validated, setValidated] = useState(false);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    const form = e.currentTarget;
    if (form.checkValidity() === false) {
      e.stopPropagation();
    } else {
      await axios
        .post(ApiV2.userLogin, { username, password })
        .then((res) => {
          const result = res.data;
          if (result.msg === "Error") {
            const err = `<p>${result.data}</>`;
            setErrMsg(err);
            setIsError(true);
          } else {
            const isLogin = true;
            props.handleSubmit(result.data, isLogin);
          }
        })
        .catch((err) => {
          setErrMsg(err.response.data.message);
          setIsError(true);
        });
    }

    setIsLoading(false);
    setValidated(true);
  };

  if (props.isLogin) {
    return <Redirect to={"/"} />;
  } else {
    return (
      <div className="main-container">
        <div className="container">
          <LoginWrapper>
            <h1>Login</h1>
            {isError && (
              <div
                dangerouslySetInnerHTML={{ __html: errMsg }}
                style={{ marginTop: 20 }}
              />
            )}
            <Form
              noValidate
              validated={validated}
              onSubmit={handleSubmit}
              style={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                width: "40%",
              }}
            >
              <Form.Group as={Col} md="12" controlId="groupUsername">
                <CustomInput
                  controlId="usernameInput"
                  label="Username"
                  required={true}
                  placeholder="Username"
                  onChange={(e) => setUserName(e.target.value)}
                  feedbackMsg="Username cannot be empty."
                />
              </Form.Group>
              <Form.Group as={Col} md="12" controlId="groupPassword">
                <CustomInput
                  controlId="passwordInput"
                  label="Password"
                  required={true}
                  type="password"
                  placeholder="Password"
                  onChange={(e) => setPassword(e.target.value)}
                  feedbackMsg="Password cannot be empty."
                />
              </Form.Group>
              <CustomButton label={"Log In"} isLoading={isLoading} />
            </Form>
          </LoginWrapper>
        </div>
      </div>
    );
  }
};

export default Login;

const LoginWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;
