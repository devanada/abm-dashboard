import React, { useState, useEffect } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import { CustomLoading } from "./Components/CustomLoading";
import Sidebar from "./Components/Sidebar";
import Header from "./Components/Header";
import { getToken, setUserSession, removeUserSession } from "./Utils/Common";
import Login from "./Pages/Login";
import SignUp from "./Pages/SignUp";
import Main from "./Pages/Main/Main";
import LogPage from "./Pages/Main/Log";
import ZoomPage from "./Pages/Main/Zoom";
import UserMain from "./Pages/Main/User/Main";
import UserCreate from "./Pages/Main/User/Create";
import UserUpdate from "./Pages/Main/User/Update";
import JurusanMain from "./Pages/Main/Jurusan/Main";
import JurusanCreate from "./Pages/Main/Jurusan/Create";
import JurusanUpdate from "./Pages/Main/Jurusan/Update";
import KelasMain from "./Pages/Main/Kelas/Main";
import KelasCreate from "./Pages/Main/Kelas/Create";
import KelasUpdate from "./Pages/Main/Kelas/Update";
import MataKuliahMain from "./Pages/Main/MataKuliah/Main";
import MataKuliahCreate from "./Pages/Main/MataKuliah/Create";
import MataKuliahUpdate from "./Pages/Main/MataKuliah/Update";
import MahasiswaMain from "./Pages/Main/Mahasiswa/Main";
import MahasiswaMbkm from "./Pages/Main/Mahasiswa/MBKM";
import MahasiswaCreate from "./Pages/Main/Mahasiswa/Create";
import MahasiswaUpdate from "./Pages/Main/Mahasiswa/Update";
import PengajarMain from "./Pages/Main/Pengajar/Main";
import PengajarCreate from "./Pages/Main/Pengajar/Create";
import PengajarUpdate from "./Pages/Main/Pengajar/Update";
import ProdiMain from "./Pages/Main/Prodi/Main";
import ProdiCreate from "./Pages/Main/Prodi/Create";
import ProdiUpdate from "./Pages/Main/Prodi/Update";
import SemesterMain from "./Pages/Main/Semester/Main";
import SemesterCreate from "./Pages/Main/Semester/Create";
import SemesterUpdate from "./Pages/Main/Semester/Update";
import TahunAjaranMain from "./Pages/Main/TahunAjaran/Main";
import TahunAjaranCreate from "./Pages/Main/TahunAjaran/Create";
import TahunAjaranUpdate from "./Pages/Main/TahunAjaran/Update";
import MengajarMain from "./Pages/Main/Mengajar/Main";
import NotFound from "./Pages/NotFound";
import "./Styles/App.scss";
import "./Styles/Container.css";
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  // eslint-disable-next-line
  const [toggled, setToggled] = useState(false);
  const [isLogin, setIsLogin] = useState(false);
  const [isReady, setIsReady] = useState(false);

  useEffect(() => {
    const token = getToken();
    if (token !== null) {
      setIsLogin(true);
    } else {
      setIsLogin(false);
    }
    setIsReady(true);
  }, []);

  const handleToggleSidebar = (value) => {
    setToggled(value);
  };

  const handleSubmit = async (data, isLogin) => {
    setUserSession({ name: data.api_name, key: data.api_key }, data);
    setIsLogin(isLogin);
  };

  const handleLogout = async () => {
    removeUserSession();
    setIsLogin(false);
  };

  if (isReady) {
    return (
      <BrowserRouter>
        <div style={{ display: "flex", height: "100%" }}>
          {isLogin && (
            <Sidebar
              toggled={toggled}
              handleToggleSidebar={handleToggleSidebar}
            />
          )}
          <Switch>
            <Route path="/login">
              {(props) => (
                <div className="parent">
                  <Login
                    {...props}
                    handleSubmit={handleSubmit}
                    isLogin={isLogin}
                  />
                </div>
              )}
            </Route>
            <Route path="/signup">
              {(props) => (
                <div className="parent">
                  <SignUp {...props} handleSubmit={handleSubmit} />
                </div>
              )}
            </Route>
            <Route exact path="/">
              {(props) => (
                <div className="parent">
                  <Header
                    onclick={() => handleToggleSidebar(true)}
                    handleLogout={handleLogout}
                  />
                  <Main {...props} isLogin={isLogin} />
                </div>
              )}
            </Route>
            <Route path="/log-activity">
              {(props) => (
                <div className="parent">
                  <Header
                    onclick={() => handleToggleSidebar(true)}
                    handleLogout={handleLogout}
                  />
                  <LogPage {...props} isLogin={isLogin} />
                </div>
              )}
            </Route>
            <Route path="/zoom-activity">
              {(props) => (
                <div className="parent">
                  <Header
                    onclick={() => handleToggleSidebar(true)}
                    handleLogout={handleLogout}
                  />
                  <ZoomPage {...props} isLogin={isLogin} />
                </div>
              )}
            </Route>
            <Route exact path="/tahun-ajaran">
              {(props) => (
                <div className="parent">
                  <Header
                    onclick={() => handleToggleSidebar(true)}
                    handleLogout={handleLogout}
                  />
                  <TahunAjaranMain {...props} isLogin={isLogin} />
                </div>
              )}
            </Route>
            <Route path="/tahun-ajaran/create">
              {(props) => (
                <div className="parent">
                  <Header
                    onclick={() => handleToggleSidebar(true)}
                    handleLogout={handleLogout}
                  />
                  <TahunAjaranCreate {...props} isLogin={isLogin} />
                </div>
              )}
            </Route>
            <Route path="/tahun-ajaran/:id/update">
              {(props) => (
                <div className="parent">
                  <Header
                    onclick={() => handleToggleSidebar(true)}
                    handleLogout={handleLogout}
                  />
                  <TahunAjaranUpdate {...props} isLogin={isLogin} />
                </div>
              )}
            </Route>
            <Route exact path="/jurusan">
              {(props) => (
                <div className="parent">
                  <Header
                    onclick={() => handleToggleSidebar(true)}
                    handleLogout={handleLogout}
                  />
                  <JurusanMain {...props} isLogin={isLogin} />
                </div>
              )}
            </Route>
            <Route path="/jurusan/create">
              {(props) => (
                <div className="parent">
                  <Header
                    onclick={() => handleToggleSidebar(true)}
                    handleLogout={handleLogout}
                  />
                  <JurusanCreate {...props} isLogin={isLogin} />
                </div>
              )}
            </Route>
            <Route path="/jurusan/:id">
              {(props) => (
                <div className="parent">
                  <Header
                    onclick={() => handleToggleSidebar(true)}
                    handleLogout={handleLogout}
                  />
                  <JurusanUpdate {...props} isLogin={isLogin} />
                </div>
              )}
            </Route>
            <Route exact path="/user">
              {(props) => (
                <div className="parent">
                  <Header
                    onclick={() => handleToggleSidebar(true)}
                    handleLogout={handleLogout}
                  />
                  <UserMain {...props} isLogin={isLogin} />
                </div>
              )}
            </Route>
            <Route path="/user/create">
              {(props) => (
                <div className="parent">
                  <Header
                    onclick={() => handleToggleSidebar(true)}
                    handleLogout={handleLogout}
                  />
                  <UserCreate {...props} isLogin={isLogin} />
                </div>
              )}
            </Route>
            <Route path="/user/:id">
              {(props) => (
                <div className="parent">
                  <Header
                    onclick={() => handleToggleSidebar(true)}
                    handleLogout={handleLogout}
                  />
                  <UserUpdate {...props} isLogin={isLogin} />
                </div>
              )}
            </Route>
            <Route exact path="/prodi">
              {(props) => (
                <div className="parent">
                  <Header
                    onclick={() => handleToggleSidebar(true)}
                    handleLogout={handleLogout}
                  />
                  <ProdiMain {...props} isLogin={isLogin} />
                </div>
              )}
            </Route>
            <Route path="/prodi/create">
              {(props) => (
                <div className="parent">
                  <Header
                    onclick={() => handleToggleSidebar(true)}
                    handleLogout={handleLogout}
                  />
                  <ProdiCreate {...props} isLogin={isLogin} />
                </div>
              )}
            </Route>
            <Route path="/prodi/:id/update">
              {(props) => (
                <div className="parent">
                  <Header
                    onclick={() => handleToggleSidebar(true)}
                    handleLogout={handleLogout}
                  />
                  <ProdiUpdate {...props} isLogin={isLogin} />
                </div>
              )}
            </Route>
            <Route exact path="/semester">
              {(props) => (
                <div className="parent">
                  <Header
                    onclick={() => handleToggleSidebar(true)}
                    handleLogout={handleLogout}
                  />
                  <SemesterMain {...props} isLogin={isLogin} />
                </div>
              )}
            </Route>
            <Route path="/semester/create">
              {(props) => (
                <div className="parent">
                  <Header
                    onclick={() => handleToggleSidebar(true)}
                    handleLogout={handleLogout}
                  />
                  <SemesterCreate {...props} isLogin={isLogin} />
                </div>
              )}
            </Route>
            <Route path="/semester/:id/update">
              {(props) => (
                <div className="parent">
                  <Header
                    onclick={() => handleToggleSidebar(true)}
                    handleLogout={handleLogout}
                  />
                  <SemesterUpdate {...props} isLogin={isLogin} />
                </div>
              )}
            </Route>
            <Route exact path="/kelas">
              {(props) => (
                <div className="parent">
                  <Header
                    onclick={() => handleToggleSidebar(true)}
                    handleLogout={handleLogout}
                  />
                  <KelasMain {...props} isLogin={isLogin} />
                </div>
              )}
            </Route>
            <Route path="/kelas/create">
              {(props) => (
                <div className="parent">
                  <Header
                    onclick={() => handleToggleSidebar(true)}
                    handleLogout={handleLogout}
                  />
                  <KelasCreate {...props} isLogin={isLogin} />
                </div>
              )}
            </Route>
            <Route path="/kelas/:id/update">
              {(props) => (
                <div className="parent">
                  <Header
                    onclick={() => handleToggleSidebar(true)}
                    handleLogout={handleLogout}
                  />
                  <KelasUpdate {...props} isLogin={isLogin} />
                </div>
              )}
            </Route>
            <Route exact path="/mata_kuliah">
              {(props) => (
                <div className="parent">
                  <Header
                    onclick={() => handleToggleSidebar(true)}
                    handleLogout={handleLogout}
                  />
                  <MataKuliahMain {...props} isLogin={isLogin} />
                </div>
              )}
            </Route>
            <Route path="/mata_kuliah/create">
              {(props) => (
                <div className="parent">
                  <Header
                    onclick={() => handleToggleSidebar(true)}
                    handleLogout={handleLogout}
                  />
                  <MataKuliahCreate {...props} isLogin={isLogin} />
                </div>
              )}
            </Route>
            <Route path="/mata_kuliah/:id/update">
              {(props) => (
                <div className="parent">
                  <Header
                    onclick={() => handleToggleSidebar(true)}
                    handleLogout={handleLogout}
                  />
                  <MataKuliahUpdate {...props} isLogin={isLogin} />
                </div>
              )}
            </Route>
            <Route exact path="/mengajar">
              {(props) => (
                <div className="parent">
                  <Header
                    onclick={() => handleToggleSidebar(true)}
                    handleLogout={handleLogout}
                  />
                  <MengajarMain {...props} isLogin={isLogin} />
                </div>
              )}
            </Route>
            <Route exact path="/pengajar">
              {(props) => (
                <div className="parent">
                  <Header
                    onclick={() => handleToggleSidebar(true)}
                    handleLogout={handleLogout}
                  />
                  <PengajarMain {...props} isLogin={isLogin} />
                </div>
              )}
            </Route>
            <Route path="/pengajar/create">
              {(props) => (
                <div className="parent">
                  <Header
                    onclick={() => handleToggleSidebar(true)}
                    handleLogout={handleLogout}
                  />
                  <PengajarCreate {...props} isLogin={isLogin} />
                </div>
              )}
            </Route>
            <Route path="/pengajar/:id/update">
              {(props) => (
                <div className="parent">
                  <Header
                    onclick={() => handleToggleSidebar(true)}
                    handleLogout={handleLogout}
                  />
                  <PengajarUpdate {...props} isLogin={isLogin} />
                </div>
              )}
            </Route>
            <Route exact path="/mahasiswa">
              {(props) => (
                <div className="parent">
                  <Header
                    onclick={() => handleToggleSidebar(true)}
                    handleLogout={handleLogout}
                  />
                  <MahasiswaMain {...props} isLogin={isLogin} />
                </div>
              )}
            </Route>
            <Route exact path="/mahasiswa/:ismbkm">
              {(props) => (
                <div className="parent">
                  <Header
                    onclick={() => handleToggleSidebar(true)}
                    handleLogout={handleLogout}
                  />
                  <MahasiswaMbkm {...props} isLogin={isLogin} />
                </div>
              )}
            </Route>
            <Route path="/mahasiswa/create">
              {(props) => (
                <div className="parent">
                  <Header
                    onclick={() => handleToggleSidebar(true)}
                    handleLogout={handleLogout}
                  />
                  <MahasiswaCreate {...props} isLogin={isLogin} />
                </div>
              )}
            </Route>
            <Route path="/mahasiswa/:id/update">
              {(props) => (
                <div className="parent">
                  <Header
                    onclick={() => handleToggleSidebar(true)}
                    handleLogout={handleLogout}
                  />
                  <MahasiswaUpdate {...props} isLogin={isLogin} />
                </div>
              )}
            </Route>
            <Route>
              {(props) => (
                <div className="parent">
                  {isLogin && (
                    <Header
                      onclick={() => handleToggleSidebar(true)}
                      handleLogout={handleLogout}
                    />
                  )}
                  <NotFound {...props} isLogin={isLogin} />
                </div>
              )}
            </Route>
          </Switch>
        </div>
      </BrowserRouter>
    );
  } else {
    return (
      <div>
        <CustomLoading visible={true} />
      </div>
    );
  }
}

export default App;
