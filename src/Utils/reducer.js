/* eslint-disable default-case */
export const reducer = (prevState, action) => {
  switch (action.type) {
    case "OCAS":
      return {
        ...prevState,
        ocas: action.ocas,
      };
  }
};

export const initialState = {
  ocas: [],
};
