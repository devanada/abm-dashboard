/* eslint-disable no-unused-vars */
/* eslint-disable import/no-anonymous-default-export */
const baseUrl = "http://103.145.148.11:8000/api";
export default {
  // Prodi
  listProdi: baseUrl + "/prodis",
  detailProdi: baseUrl + "/prodi/",
  // Student
  listStudentByAngkatan: baseUrl + "/studentsByAngkatan/",
  listStudentFromKelas: baseUrl + "/class/students",
  listStudentFromMBKM: baseUrl + "/class/students/mbkm",
  detailStudent: baseUrl + "/student/",
  // Course
  listCourses: baseUrl + "/courses",
  detailCourses: baseUrl + "/course",
  // Kelas
  listClasses: baseUrl + "/classes",
  detailClasses: baseUrl + "/class",
  // Teacher
  listTeachers: baseUrl + "/teachers",
  detailTeacher: baseUrl + "/teacher",
  teachings: baseUrl + "/teachings",
};
