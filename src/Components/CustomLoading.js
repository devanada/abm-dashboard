import React from "react";
import { Spinner } from "react-bootstrap";

const CustomLoading = ({ visible }) => {
  return (
    <>
      <div style={{ display: "flex", justifyContent: "center" }}>
        <Spinner animation="border" role="status" />
      </div>
    </>
  );
};
export { CustomLoading };
