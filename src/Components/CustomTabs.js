import React, { useState } from "react";
import { Tabs, Tab } from "react-bootstrap";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import IconButton from "@material-ui/core/IconButton";
import Button from "@material-ui/core/Button";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import TextField from "@mui/material/TextField";
import ClearIcon from "@mui/icons-material/Clear";
import SearchIcon from "@mui/icons-material/Search";
import FilterListIcon from "@mui/icons-material/FilterList";
import Tooltip from "@mui/material/Tooltip";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
// import PropTypes from "prop-types";

import CustomTableHead from "./TableHead";

const useStyles = makeStyles({
  root: {
    width: "100%",
  },
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1,
  },
  container: {
    // maxHeight: 440,
  },
});

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

function escapeRegExp(value) {
  return value.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
}

const ITEM_HEIGHT = 48;

const CustomTabs = (props) => {
  const {
    columnsOcas,
    columns,
    ocas,
    data,
    onClick1,
    onClick2,
    onClick3,
    onClick4,
    onClick5,
    options,
    menu,
  } = props;
  const classes = useStyles();
  const [order, setOrder] = useState("asc");
  const [orderBy, setOrderBy] = useState("id");
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [searchText1, setSearchText1] = useState("");
  const [searchText2, setSearchText2] = useState("");
  const [selected, setSelected] = useState("");
  const [datas, setDatas] = useState(data);
  const [ocass, setOcass] = useState(ocas);
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = (e, source) => {
    if (e.target.textContent !== "") {
      if (selected !== e.target.textContent) {
        requestSearch(e.target.textContent, source, "filter");
        setSelected(e.target.textContent);
        setAnchorEl(null);
      } else {
        requestSearch("", source, "filter");
        setSelected("");
        setAnchorEl(null);
      }
    } else {
      setAnchorEl(null);
    }
  };

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const requestSearch = (searchValue, event, from) => {
    if (event === "ocas") {
      setSearchText1(from !== "filter" ? searchValue : "");
      const searchRegex = new RegExp(escapeRegExp(searchValue), "i");
      const filteredRows = ocas.filter((row) => {
        return Object.keys(row).some((field) => {
          return searchRegex.test(row[field].toString());
        });
      });
      setOcass(filteredRows);
    } else {
      setSearchText2(from !== "filter" ? searchValue : "");
      const searchRegex = new RegExp(escapeRegExp(searchValue), "i");
      const filteredRows = data.filter((row) => {
        return Object.keys(row).some((field) => {
          return searchRegex.test(row[field].toString());
        });
      });
      setDatas(filteredRows);
    }
  };

  return (
    <>
      <Tabs defaultActiveKey="ocas" id="abm-tab" className="mb-3">
        <Tab eventKey="ocas" title="Ocas">
          <TableContainer className={classes.container}>
            {options && (
              <>
                <Tooltip title="Filter list">
                  <IconButton
                    aria-label="more"
                    id="long-button"
                    aria-controls="long-menu"
                    aria-expanded={open ? "true" : undefined}
                    aria-haspopup="true"
                    onClick={handleClick}
                  >
                    <FilterListIcon />
                  </IconButton>
                </Tooltip>
                <Menu
                  id="long-menu"
                  MenuListProps={{
                    "aria-labelledby": "long-button",
                  }}
                  anchorEl={anchorEl}
                  open={open}
                  onClose={(e) => handleClose(e, "dashboard")}
                  PaperProps={{
                    style: {
                      maxHeight: ITEM_HEIGHT * 4.5,
                      width: "20ch",
                    },
                  }}
                >
                  {options.map((option) => (
                    <MenuItem
                      key={option}
                      selected={option === selected}
                      onClick={(e) => handleClose(e, "dashboard")}
                    >
                      {option}
                    </MenuItem>
                  ))}
                </Menu>
              </>
            )}
            <TextField
              variant="standard"
              value={searchText1}
              onChange={(e) => requestSearch(e.target.value, "ocas")}
              placeholder="Search…"
              InputProps={{
                startAdornment: <SearchIcon fontSize="small" />,
                endAdornment: (
                  <IconButton
                    title="Clear"
                    aria-label="Clear"
                    size="small"
                    style={{ visibility: searchText1 ? "visible" : "hidden" }}
                    onClick={() => requestSearch("", "ocas")}
                  >
                    <ClearIcon fontSize="small" />
                  </IconButton>
                ),
              }}
              sx={{
                width: {
                  xs: 1,
                  sm: "auto",
                },
                m: (theme) => theme.spacing(1, 0.5, 1.5),
                "& .MuiSvgIcon-root": {
                  mr: 0.5,
                },
                "& .MuiInput-underline:before": {
                  borderBottom: 1,
                  borderColor: "divider",
                },
              }}
            />
            <Table aria-label="sticky table">
              <CustomTableHead
                classes={classes}
                order={order}
                orderBy={orderBy}
                onRequestSort={handleRequestSort}
                rowCount={ocass.length}
                columns={columnsOcas}
              />
              <TableBody>
                {stableSort(ocass, getComparator(order, orderBy))
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row, index) => {
                    return (
                      <TableRow hover role="checkbox" tabIndex={-1} key={index}>
                        {columnsOcas.map((column) => {
                          const value = row[column.id];
                          return (
                            <TableCell key={column.id} align={column.align}>
                              {menu !== "kelas"
                                ? column.id === "migration"
                                  ? onClick3 && (
                                      <Button
                                        variant="outlined"
                                        onClick={() => onClick3(row)}
                                      >
                                        Migrasi
                                      </Button>
                                    )
                                  : value
                                : value}
                            </TableCell>
                          );
                        })}
                      </TableRow>
                    );
                  })}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[10, 25, 100]}
            component="div"
            count={ocass.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Tab>
        <Tab eventKey="dashboard" title="Dashboard">
          <TableContainer className={classes.container}>
            {options && (
              <>
                <Tooltip title="Filter list">
                  <IconButton
                    aria-label="more"
                    id="long-button"
                    aria-controls="long-menu"
                    aria-expanded={open ? "true" : undefined}
                    aria-haspopup="true"
                    onClick={handleClick}
                  >
                    <FilterListIcon />
                  </IconButton>
                </Tooltip>
                <Menu
                  id="long-menu"
                  MenuListProps={{
                    "aria-labelledby": "long-button",
                  }}
                  anchorEl={anchorEl}
                  open={open}
                  onClose={(e) => handleClose(e, "dashboard")}
                  PaperProps={{
                    style: {
                      maxHeight: ITEM_HEIGHT * 4.5,
                      width: "20ch",
                    },
                  }}
                >
                  {options.map((option) => (
                    <MenuItem
                      key={option}
                      selected={option === selected}
                      onClick={(e) => handleClose(e, "dashboard")}
                    >
                      {option}
                    </MenuItem>
                  ))}
                </Menu>
              </>
            )}
            <TextField
              variant="standard"
              value={searchText2}
              onChange={(e) => requestSearch(e.target.value, "dashboard")}
              placeholder="Search…"
              InputProps={{
                startAdornment: <SearchIcon fontSize="small" />,
                endAdornment: (
                  <IconButton
                    title="Clear"
                    aria-label="Clear"
                    size="small"
                    style={{ visibility: searchText2 ? "visible" : "hidden" }}
                    onClick={() => requestSearch("", "dashboard")}
                  >
                    <ClearIcon fontSize="small" />
                  </IconButton>
                ),
              }}
              sx={{
                width: {
                  xs: 1,
                  sm: "auto",
                },
                m: (theme) => theme.spacing(1, 0.5, 1.5),
                "& .MuiSvgIcon-root": {
                  mr: 0.5,
                },
                "& .MuiInput-underline:before": {
                  borderBottom: 1,
                  borderColor: "divider",
                },
              }}
            />
            <Table aria-label="sticky table">
              <CustomTableHead
                classes={classes}
                order={order}
                orderBy={orderBy}
                onRequestSort={handleRequestSort}
                rowCount={datas.length}
                showButton={true}
                columns={columns}
                onClick1={onClick1}
                onClick2={onClick2}
              />
              <TableBody>
                {stableSort(datas, getComparator(order, orderBy))
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row, index) => {
                    return (
                      <TableRow hover role="checkbox" tabIndex={-1} key={index}>
                        {columns.map((column) => {
                          const value = row[column.id];
                          return (
                            <TableCell key={column.id} align={column.align}>
                              {menu !== "kelas" ? (
                                column.id === "migration" ? (
                                  <Button
                                    variant="outlined"
                                    color="secondary"
                                    disabled={value === "sudah" ? true : false}
                                    onClick={() => onClick4(row)}
                                  >
                                    {value}
                                  </Button>
                                ) : column.id === "enroll" ? (
                                  onClick5 ? (
                                    <Button
                                      variant="outlined"
                                      // disabled={
                                      //   value === "sudah" ? true : false
                                      // }
                                      color={
                                        value === "sudah"
                                          ? "primary"
                                          : "secondary"
                                      }
                                      onClick={() => onClick5(row)}
                                    >
                                      {value}
                                    </Button>
                                  ) : (
                                    value
                                  )
                                ) : (
                                  value
                                )
                              ) : (
                                value
                              )}
                            </TableCell>
                          );
                        })}
                        {onClick1 && (
                          <TableCell>
                            <IconButton
                              color="secondary"
                              aria-label="delete"
                              onClick={() => onClick1(row)}
                            >
                              <DeleteIcon />
                            </IconButton>
                          </TableCell>
                        )}
                        {onClick2 && (
                          <TableCell>
                            <IconButton
                              color="primary"
                              aria-label="edit"
                              onClick={() => onClick2(row)}
                            >
                              <EditIcon />
                            </IconButton>
                          </TableCell>
                        )}
                      </TableRow>
                    );
                  })}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[10, 25, 100]}
            component="div"
            count={datas.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Tab>
      </Tabs>
    </>
  );
};

export { CustomTabs };
