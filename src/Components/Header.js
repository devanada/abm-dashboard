import React, { useState, useEffect, useRef } from "react";
import { FaBars } from "react-icons/fa";
import { useHistory } from "react-router-dom";
import { getUser } from "../Utils/Common";
import { useDetectOutsideClick } from "../Utils/useDetectOutsideClick";
import "../Styles/Header.css";

const Header = ({ onclick, handleLogout }) => {
  let history = useHistory();
  const [name, setName] = useState(null);
  const dropdownRef = useRef(null);
  const [isActive, setIsActive] = useDetectOutsideClick(dropdownRef, false);
  const onClick = () => setIsActive(!isActive);

  useEffect(() => {
    const user = getUser();
    if (user !== null) {
      setName(user.name);
    }
  }, []);

  return (
    <div className={name !== null ? "header" : "header2"}>
      {name !== null && (
        <div className="btn-toggle" onClick={onclick}>
          <FaBars />
        </div>
      )}
      {name !== null ? (
        <div className="header-right">
          <button onClick={onClick} className="menu-trigger">
            <img
              src="https://steamcdn-a.akamaihd.net/steamcommunity/public/images/avatars/df/df7789f313571604c0e4fb82154f7ee93d9989c6.jpg"
              alt="User avatar"
            />
            <p style={{ marginLeft: 10, marginTop: "revert", color: "#fff" }}>
              {name}
            </p>
          </button>
          <nav
            ref={dropdownRef}
            className={`menu ${isActive ? "active" : "inactive"}`}
          >
            <ul>
              <li>
                <p onClick={() => handleLogout()}>Logout</p>
              </li>
            </ul>
          </nav>
        </div>
      ) : (
        <div className="header-right">
          <button onClick={onClick} className="menu-trigger">
            <a href={"/login"} onClick={() => history.push("/login")}>
              Login
            </a>
          </button>
        </div>
      )}
    </div>
  );
};

export default Header;
