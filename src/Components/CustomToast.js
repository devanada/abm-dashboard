import React from "react";
import { ToastContainer, Toast } from "react-bootstrap";

const CustomToast = ({ onClose, show, headerText, bodyText }) => {
  return (
    <>
      <ToastContainer className="p-3" position="bottom-end">
        <Toast onClose={onClose} show={show} delay={10000} autohide>
          <Toast.Header>
            <img
              src="holder.js/20x20?text=%20"
              className="rounded me-2"
              alt=""
            />
            <strong className="me-auto">{headerText}</strong>
          </Toast.Header>
          <Toast.Body>{bodyText}</Toast.Body>
        </Toast>
      </ToastContainer>
    </>
  );
};
export { CustomToast };
