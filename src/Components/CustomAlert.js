import React from "react";
import { Modal, Button, Spinner } from "react-bootstrap";

const CustomAlert = ({
  show,
  onHide,
  modalBody,
  onClickNo,
  onClickYes,
  isLoading,
  showNoButton,
}) => {
  return (
    <>
      <Modal
        show={show}
        onHide={onHide}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Body>
          <p>{modalBody}</p>
        </Modal.Body>
        <Modal.Footer>
          {showNoButton && (
            <Button variant="danger" onClick={onClickNo}>
              Tidak
            </Button>
          )}
          <Button onClick={onClickYes} disabled={isLoading}>
            {isLoading ? (
              <Spinner animation="border" role="status" size="sm" />
            ) : (
              "Ya"
            )}
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};
export { CustomAlert };
