import React from "react";
import {
  ProSidebar,
  Menu,
  MenuItem,
  SubMenu,
  SidebarHeader,
  SidebarContent,
} from "react-pro-sidebar";
import { Link, useHistory } from "react-router-dom";

import sidebarBg from "../Assets/bg1.jpg";
import logo from "../Assets/P3M-1.png";

const Sidebar = (props) => {
  let history = useHistory();

  return (
    <>
      <ProSidebar
        image={props.image ? sidebarBg : false}
        collapsed={props.collapsed}
        toggled={props.toggled}
        breakPoint="md"
        onToggle={props.handleToggleSidebar}
      >
        <SidebarHeader>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              padding: "24px",
              textTransform: "uppercase",
              fontWeight: "bold",
              letterSpacing: "1px",
              overflow: "hidden",
              textOverflow: "ellipsis",
              whiteSpace: "nowrap",
              alignItems: "center",
            }}
          >
            <a
              href={"/"}
              onClick={() => history.push("/")}
              style={{
                textDecoration: "none",
                color: "black",
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <img
                style={{ alignItems: "center" }}
                width={"50%"}
                src={logo}
                alt="logo"
              />
              <p style={{ fontSize: 14, textAlign: "center", color: "#fff" }}>
                Admin Dashboard
              </p>
            </a>
          </div>
        </SidebarHeader>
        <SidebarContent>
          <Menu>
            {/* <SubMenu
              title="Tahun Ajaran"
              style={{ fontSize: 20 }}
              defaultOpen={["/tahun-ajaran", "/tahun-ajaran/create"].includes(
                history.location.pathname
              )}
            >
              <MenuItem>
                Daftar Tahun Ajaran
                <Link to="/tahun-ajaran" />
              </MenuItem>
              <MenuItem>
                Buat Tahun Ajaran Baru
                <Link to="/tahun-ajaran/create" />
              </MenuItem>
            </SubMenu> */}
            {/* <SubMenu
              title="Semester"
              style={{ fontSize: 20 }}
              defaultOpen={["/semester", "/semester/create"].includes(
                history.location.pathname
              )}
            >
              <MenuItem>
                Daftar Semester
                <Link to="/semester" />
              </MenuItem>
              <MenuItem>
                Buat Semester Baru
                <Link to="/semester/create" />
              </MenuItem>
            </SubMenu> */}
            {/* <SubMenu
              title="Prodi"
              style={{ fontSize: 20 }}
              defaultOpen={["/prodi", "/prodi/create"].includes(
                history.location.pathname
              )}
            >
              <MenuItem>
                Daftar Prodi
                <Link to="/prodi" />
              </MenuItem>
              <MenuItem>
                Buat Prodi Baru
                <Link to="/prodi/create" />
              </MenuItem>
            </SubMenu> */}
            {/* <SubMenu
              title="Jurusan"
              style={{ fontSize: 20 }}
              defaultOpen={["/jurusan", "/jurusan/create"].includes(
                history.location.pathname
              )}
            >
              <MenuItem>
                Daftar Jurusan
                <Link to="/jurusan" />
              </MenuItem>
              <MenuItem>
                Buat Jurusan Baru
                <Link to="/jurusan/create" />
              </MenuItem>
            </SubMenu> */}
            {/* <SubMenu
              title="Mata Kuliah"
              style={{ fontSize: 20 }}
              defaultOpen={["/mata_kuliah", "/mata_kuliah/create"].includes(
                history.location.pathname
              )}
            >
              <MenuItem>
                Daftar Mata Kuliah
                <Link to="/mata_kuliah" />
              </MenuItem>
              <MenuItem>
                Buat Mata Kuliah Baru
                <Link to="/mata_kuliah/create" />
              </MenuItem>
            </SubMenu> */}
            <SubMenu
              title="Kelas"
              style={{ fontSize: 20 }}
              defaultOpen={["/kelas", "/kelas/create"].includes(
                history.location.pathname
              )}
            >
              <MenuItem>
                Daftar Kelas
                <Link to="/kelas" />
              </MenuItem>
              {/* <MenuItem>
                Buat Kelas Baru
                <Link to="/kelas/create" />
              </MenuItem> */}
            </SubMenu>
            <SubMenu
              title="Mahasiswa"
              style={{ fontSize: 20 }}
              defaultOpen={["/mahasiswa", "/mahasiswa/mbkm"].includes(
                history.location.pathname
              )}
            >
              <MenuItem>
                Daftar Mahasiswa
                <Link to="/mahasiswa" />
              </MenuItem>
              <MenuItem onClick={() => alert("On Development")}>
                Daftar Mahasiswa MBKM
                {/* <Link to="/mahasiswa/mbkm" onClick={() => console.log("test")}/> */}
              </MenuItem>
            </SubMenu>
            <SubMenu
              title="Pengajar"
              style={{ fontSize: 20 }}
              defaultOpen={["/pengajar", "/pengajar/create"].includes(
                history.location.pathname
              )}
            >
              <MenuItem>
                Daftar Pengajar
                <Link to="/pengajar" />
              </MenuItem>
              {/* <MenuItem>
                Buat Pengajar Baru
                <Link to="/pengajar/create" />
              </MenuItem> */}
            </SubMenu>
            <SubMenu
              title="Data Mengajar"
              style={{ fontSize: 20 }}
              defaultOpen={["/mengajar"].includes(history.location.pathname)}
            >
              <MenuItem>
                Data Mengajar
                <Link to="/mengajar" />
              </MenuItem>
            </SubMenu>
            <SubMenu
              title="User"
              style={{ fontSize: 20 }}
              defaultOpen={["/user", "/user/create"].includes(
                history.location.pathname
              )}
            >
              <MenuItem>
                Daftar User
                <Link to="/user" />
              </MenuItem>
              <MenuItem>
                Buat User Baru
                <Link to="/user/create" />
              </MenuItem>
            </SubMenu>
            <MenuItem style={{ fontSize: 20 }}>
              Aktivitas Log
              <Link to="/log-activity" />
            </MenuItem>
            <MenuItem style={{ fontSize: 20 }}>
              Aktivitas Zoom
              <Link to="/zoom-activity" />
            </MenuItem>
          </Menu>
        </SidebarContent>
      </ProSidebar>
    </>
  );
};

export default Sidebar;
