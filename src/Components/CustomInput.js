import React from "react";
import { Form } from "react-bootstrap";
import FloatingLabel from "react-bootstrap/FloatingLabel";

const CustomInput = ({
  controlId,
  label,
  required,
  type,
  placeholder,
  value,
  onChange,
  feedbackMsg,
}) => {
  return (
    <>
      <FloatingLabel controlId={controlId} label={label} className="mb-3">
        <Form.Control
          required={required}
          type={type}
          placeholder={placeholder}
          value={value}
          onChange={onChange}
        />
        <Form.Control.Feedback type="invalid">
          {feedbackMsg}
        </Form.Control.Feedback>
      </FloatingLabel>
    </>
  );
};
export { CustomInput };
