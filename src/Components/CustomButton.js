import React from "react";
import { Button, Spinner } from "react-bootstrap";

const CustomButton = ({ label, isLoading }) => {
  return (
    <>
      <div className="d-grid gap-2" style={{ width: "100%" }}>
        <Button variant="primary" type="submit" size="lg">
          {isLoading ? <Spinner animation="border" role="status" /> : label}
        </Button>
      </div>
    </>
  );
};
export { CustomButton };
