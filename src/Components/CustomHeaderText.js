import React, { useState } from "react";
import { IoArrowBackOutline } from "react-icons/io5";
import { useHistory } from "react-router-dom";

function CustomHeaderText(props) {
  let history = useHistory();
  const [title] = useState(props.title);
  const [showBack] = useState(props.showBack);

  const handleBack = async (e) => {
    if (history.location.key) {
      history.goBack();
    } else {
      history.push("/");
    }
  };

  return (
    <>
      <div
        style={{ display: "flex", flexDirection: "row", alignItems: "center" }}
      >
        {showBack && (
          <IoArrowBackOutline
            size={"2em"}
            style={{ cursor: "pointer", marginRight: 20 }}
            onClick={handleBack}
          />
        )}
        <h1>{title}</h1>
      </div>
    </>
  );
}
export default CustomHeaderText;
